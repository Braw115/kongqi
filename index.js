/** @format */

import { AppRegistry, Platform, StatusBar, YellowBox  } from 'react-native'
import Root from './app/root'
import { name as appName } from './app.json'
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'])
if (Platform.OS === 'android') {
  StatusBar.setTranslucent(true)
  StatusBar.setBackgroundColor('transparent')
  StatusBar.setBarStyle('dark-content')
}
AppRegistry.registerComponent(appName, () => Root)
