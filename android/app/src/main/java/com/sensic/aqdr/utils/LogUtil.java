package com.sensic.aqdr.utils;

import android.util.Log;

import com.sensic.aqdr.BuildConfig;


public class LogUtil {

    public static boolean isDebug() {
        return BuildConfig.DEBUG;
    }


    private LogUtil() {
    }

    public static void d(String tag, String desc) {
        if (isDebug())
            Log.d(tag, desc);
    }

    public static void d(String tag, String desc, Throwable tr) {
        if (isDebug())
            Log.d(tag, desc, tr);
    }

    public static void v(String tag, String desc) {
        if (isDebug())
            Log.v(tag, desc);
    }

    public static void v(String tag, String desc, Throwable tr) {
        if (isDebug())
            Log.v(tag, desc);
    }

    public static void w(String tag, String desc) {
        if (isDebug())
            Log.w(tag, desc);
    }

    public static void w(String tag, Throwable ioe) {
        if (isDebug())
            Log.w(tag, ioe);
    }

    public static void w(String tag, String desc, Throwable e) {
        if (isDebug())
            Log.w(tag, desc, e);
    }

    public static void i(String tag, String desc) {
        if (isDebug())
            Log.i(tag, desc);
    }

    public static void i(String tag, String desc, Throwable tr) {
        if (isDebug())
            Log.i(tag, desc, tr);
    }

    public static void e(String tag, String desc) {
        if (isDebug())
            Log.e(tag, desc);
    }

    public static void e(String tag, String desc, Throwable tr) {
        if (isDebug())
            Log.e(tag, desc, tr);
    }
}
