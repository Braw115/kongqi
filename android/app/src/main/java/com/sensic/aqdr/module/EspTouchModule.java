package com.sensic.aqdr.module;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.sensic.aqdr.esptouch.EspWifiAdminSimple;
import com.sensic.aqdr.esptouch.EsptouchTask;
import com.sensic.aqdr.esptouch.IEsptouchListener;
import com.sensic.aqdr.esptouch.IEsptouchResult;
import com.sensic.aqdr.esptouch.IEsptouchTask;
import com.sensic.aqdr.esptouch.task.__IEsptouchTask;
import com.sensic.aqdr.utils.LogUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EspTouchModule extends ReactContextBaseJavaModule {

    private static final String TAG = EspTouchModule.class.getSimpleName();

    private ReactApplicationContext reactContext;

    private EspWifiAdminSimple mWifiAdmin;
    private static final int PORT = 1883;
    private List<Socket> mList = null;
    private ServerSocket server = null;
    private ExecutorService mExecutorService = null;
    private ServerSocketListen serversocket;
    private String strmsg = "";
    private boolean bExit = false;

    private Promise setNetworkPromise;
    private Promise setModePromise;
    private Promise startPromise;

    public EspTouchModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        mWifiAdmin = new EspWifiAdminSimple(reactContext);
    }

    @Override
    public String getName() {
        return "EspTouchModule";
    }

    /**
     * 一键配网
     */
    @ReactMethod
    public void setNetwork(String apSsid, String apPassword, String taskResultCountStr, Promise promise) {
        this.setNetworkPromise = promise;
        if (__IEsptouchTask.DEBUG) {
            LogUtil.d(TAG, "mBtnConfirm is clicked, mEdtApSsid = " + apSsid
                    + ", " + " mEdtApPassword = " + apPassword);
        }
        new EsptouchAsyncTask3().execute(apSsid, mWifiAdmin.getWifiConnectedBssid(), apPassword, taskResultCountStr);
    }

    /**
     * 开始监听
     */
    @ReactMethod
    public void start(Promise promise) {
        this.startPromise = promise;
        if (serversocket == null) {
            serversocket = new ServerSocketListen();
            serversocket.start();
        }
    }

    /**
     * 设置模式
     */
    @ReactMethod
    public void setMode(Promise promise) {
        this.setModePromise = promise;
        LogUtil.d(TAG,"setMode");
        sendmsg("AT+MODULE=" + "0" + "\r");//"AT+MODULE=1\r"为什么要加\r 是因为String型最后带\n，单片机中\r\n
    }

    /**
     * 初始化
     */
    @ReactMethod
    public void clear() {
        try {
            if (server != null) {
                server.close();
                server = null;
            }
            if (mList != null) {
                mList.clear();
                mList = null;
            }
            mExecutorService.shutdown();
            mExecutorService = null;

            if (serversocket != null && serversocket.isAlive()) {
                serversocket.interrupt();
                serversocket = null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void onEsptoucResultAddedPerform(final IEsptouchResult result) {
        String text = result.getBssid() + " is connected to the wifi";
        LogUtil.d(TAG, text);
        setNetworkPromise.resolve(result.getBssid());
    }

    private IEsptouchListener myListener = new IEsptouchListener() {

        @Override
        public void onEsptouchResultAdded(final IEsptouchResult result) {
            onEsptoucResultAddedPerform(result);
        }
    };

    private class EsptouchAsyncTask3 extends AsyncTask<String, Void, List<IEsptouchResult>> {

        private IEsptouchTask mEsptouchTask;
        // without the lock, if the user tap confirm and cancel quickly enough,
        // the bug will arise. the reason is follows:
        // 0. task is starting created, but not finished
        // 1. the task is cancel for the task hasn't been created, it do nothing
        // 2. task is created
        // 3. Oops, the task should be cancelled, but it is running
        private final Object mLock = new Object();

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<IEsptouchResult> doInBackground(String... params) {
            int taskResultCount = -1;
            synchronized (mLock) {
                // !!!NOTICE
                String apSsid = mWifiAdmin.getWifiConnectedSsidAscii(params[0]);
                String apBssid = params[1];
                String apPassword = params[2];
                String taskResultCountStr = params[3];
                taskResultCount = Integer.parseInt(taskResultCountStr);
                mEsptouchTask = new EsptouchTask(apSsid, apBssid, apPassword, reactContext);
                mEsptouchTask.setEsptouchListener(myListener);
            }
            List<IEsptouchResult> resultList = mEsptouchTask.executeForResults(taskResultCount);
            return resultList;
        }

        @Override
        protected void onPostExecute(List<IEsptouchResult> result) {
//            mProgressDialog.getButton(DialogInterface.BUTTON_POSITIVE)
//                    .setEnabled(true);
//            mProgressDialog.getButton(DialogInterface.BUTTON_POSITIVE).setText(
//                    "Confirm");
            IEsptouchResult firstResult = result.get(0);
            // check whether the task is cancelled and no results received
            if (!firstResult.isCancelled()) {
                int count = 0;
                // max results to be displayed, if it is more than maxDisplayCount,
                // just show the count of redundant ones
                final int maxDisplayCount = 5;
                // the task received some results including cancelled while
                // executing before receiving enough results
                if (firstResult.isSuc()) {
                    StringBuilder sb = new StringBuilder();
                    for (IEsptouchResult resultInList : result) {
                        sb.append("Esptouch success, bssid = "
                                + resultInList.getBssid()
                                + ",InetAddress = "
                                + resultInList.getInetAddress()
                                .getHostAddress() + "\n");
                        count++;
                        if (count >= maxDisplayCount) {
                            break;
                        }
                    }
                    if (count < result.size()) {
                        sb.append("\nthere's " + (result.size() - count)
                                + " more result(s) without showing\n");
                    }
                    LogUtil.d(TAG,"\nthere's " + (result.size() - count)
                            + " more result(s) without showing\n");
//                    if (setNetworkPromise != null) {
//                        setNetworkPromise.resolve(sb.toString());
//                    }
                } else {
                    LogUtil.d(TAG,"Esptouch fail");
                    if (setNetworkPromise != null) {
                        setNetworkPromise.resolve("Esptouch fail");
                    }
                }
            }
        }
    }

    class ServerSocketListen extends Thread {
        public void run() {
            try {
                mList = new ArrayList<Socket>();
                server = new ServerSocket(PORT);
                mExecutorService = Executors.newCachedThreadPool();
                LogUtil.d(TAG, "服务器正在监听...");
                Socket client = null;
                while (true) {
                    if (!server.isClosed() && server != null) {
                        client = server.accept();
                        LogUtil.d(TAG,"accept:"+client.getLocalAddress());
                        if (mList != null) {
                            mList.add(client);
                        }
                        if (!mExecutorService.isShutdown() && mExecutorService != null) {
                            mExecutorService.execute(new Service(client));
                        }
                    } else {
                        if (serversocket != null && serversocket.isAlive()) {
                            serversocket.interrupt();
                            serversocket = null;
                        }
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class Service implements Runnable {
        private Socket socket;
        private BufferedReader in = null;
        public Service(Socket socket) {
            this.socket = socket;
            try {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void run() {
            try {
                while (true) {
                    if ((strmsg = in.readLine()) != null) {
                        strmsg = socket.getInetAddress().toString().replace('/', ' ') + ":" + strmsg;
                        //sendmsg(strmsg);
                          LogUtil.d(TAG, "Service:"+strmsg);
                            if (startPromise != null) {
                                startPromise.resolve(strmsg);
                                //setModePromise.resolve(strmsg);
                            }
                        if (bExit) {
                            mList.remove(socket);
                            in.close();
                            socket.close();
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendmsg(String msg) {
        int num = mList.size();
        for (int index = 0; index < num; index++) {
            Socket mSocket = mList.get(index);
            PrintWriter pout = null;
            try {
                pout = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(mSocket.getOutputStream())), true);
                pout.println(msg);
                LogUtil.d(TAG,"println");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
