const publicState = {
  loading: false,
  auth: false,
  myInfo: {},
  codeTime: 0,
  devices: [],
  defaultDevice: '',
  history: [],
  aboutUs: {},
  messages: [],
  city: '',
  cityData:{
    aqi: 0,
    pm2_5: 0,
    pm10: 0,
    quality: '优'
  },
  version: '',
  nowData: {},
  deviceStatus: {},
  weekData:[],
  orderParams:{}
}

export const publicReducer = (state = publicState, action) => {
  switch (action.type) {
    case 'SHOW_LOADING':
      return {
        ...state,
        loading: true
      }
    case 'LOGIN_SUCCESS':
      let token =  action.payload.token || state.myInfo.token
      return {
        ...state,
        auth: true,
        myInfo: {...action.payload,token}
      }
    case 'HIDE_LOADING':
      return {
        ...state,
        loading: false
      }
    case 'QUERY_DEVICE_LIST_SUCCESS':
      let macs = action.payload.map(item=>item.mac)
      let def = macs.includes(state.defaultDevice) // 检查默认mac是否在绑定列表中
      return {
        ...state,
        devices: action.payload,
        defaultDevice: def ? state.defaultDevice : macs[0]
      }
    case 'SEND_CODE_OK':
      let time = new Date().getTime() + 60000
      return {
        ...state,
        codeTime: time
      }
    case 'LOG_OUT_SUCCESS':
      return {
        ...publicState
      }
    case 'GET_HISTORY_SUCCESS':
      return {
        ...state,
        history: action.payload
      }
    case 'IS_DRAW_OPEN':
      return {
        ...state,
        isDrawOpen: action.payload
      }
    case 'GET_MESSAGES_SUCCESS':
      return {
        ...state,
        messages: action.payload
      }
    case 'GET_CITY_SUCCESS':
      return {
        ...state,
        city: action.payload
      }
    case 'SAVE_VERSION':
      return {
        ...state,
        version: action.payload
      }
    case 'GET_CITY_DATA_SUCCESS':
      return {
        ...state,
        cityData: action.payload
      }
    case 'GET_NOW_DATA_SUCCESS':
      return {
        ...state,
        nowData: action.payload
      }
    case 'GET_WEEK_DATA_SUCCESS':
      return {
        ...state,
        weekData: action.payload
      }
    case 'GET_DEVICE_STATUS_SUCCESS':
      return {
        ...state,
        deviceStatus: action.payload
      }
    case 'GET_ORDER_PARAM_SUCCESS':
      return {
        ...state,
        orderParams: action.payload
      }
    case 'GET_ABOUT_US_SUCCESS':
      return {
        ...state,
        aboutUs: action.payload
      }
    case 'CHOOSE_DEVICE':
      let devices = state.devices.map(item=>item.mac)
      let isIn = devices.includes(action.payload) // 检查默认mac是否在绑定列表中
      if( devices.length == 0 ) {
        isIn = true   //未获取到数据时使用本地存储的默认设备名称
      }
      return {
        ...state,
        defaultDevice: isIn ? action.payload : state.defaultDevice
      }
    default:
      return state
  }
}

export default publicReducer
