import * as apis from '../constants/apis'
import http from '../utils/http'
import Toast from 'react-native-toast'

// 用户账号验证
export const checkUser = (opt) => {
  return (dispatch) => {
    return http.check(apis.checkUser, 'get', opt)
      .then((res) => {
        if (res.message == 'ok') {

        } else {
          Toast.showShortBottom(res.message)
        }
        return res
      })
      .catch((error) => {
        Toast.showShortBottom(error.message)
        throw error
      })
  }
}

// 用户密码验证
export const checkPsw = (opt) => {
  return (dispatch) => {
    return http.check(apis.checkPsw, 'post', opt)
      .then((res) => {
        if (res.message == 'ok') {

        } else {
          Toast.showShortBottom(res.message)
        }
        return res
      })
      .catch((error) => {
        Toast.showShortBottom(error.message)
        throw error
      })
  }
}

// 用户手机验证
export const checkPhone = (opt) => {
  return (dispatch) => {
    return http.check(apis.checkPhone, 'get', opt)
      .then((res) => {
        if (res.message == 'ok') {

        } else {
          Toast.showShortBottom(res.message)
        }
        return res
      })
      .catch((error) => {
        Toast.showShortBottom(error.message)
        throw error
      })
  }
}

// 用户邮箱验证
export const checkMail = (opt) => {
  return (dispatch) => {
    return http.check(apis.checkMail, 'get', opt)
      .then((res) => {
        if (res.message == 'ok') {

        } else {
          Toast.showShortBottom(res.message)
        }
        return res
      })
      .catch((error) => {
        Toast.showShortBottom(error.message)
        throw error
      })
  }
}
