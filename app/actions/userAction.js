import * as apis from '../constants/apis'
import http from '../utils/http'
import Toast from 'react-native-toast'
import store from 'react-native-simple-store'
import NavigatorService from '../navigatorService'

// 显示loading
export const showLoading = () => {
  return {
    type: 'SHOW_LOADING'
  }
}

export const hideLoading = () => {
  return {
    type: 'HIDE_LOADING'
  }
}
export const sendCodeOk = () => {
  return {
    type: 'SEND_CODE_OK'
  }
}
export const loginSuccess = (res) => {
  return {
    type: 'LOGIN_SUCCESS',
    payload: res
  }
}
export const logOutSuccess = () => {
  return {
    type: 'LOG_OUT_SUCCESS'
  }
}
export const queryDeviceListSuccess = (res) => {
  return {
    type: 'QUERY_DEVICE_LIST_SUCCESS',
    payload: res
  }
}

export const sendRegister = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.getUserMsg, 'post', opt)
      .then((res) => {
        dispatch(hideLoading())
        Toast.showShortBottom('注册成功')
        return res
      })
      .catch((error) => {
        Toast.showShortBottom(error.message)
        dispatch(hideLoading())
        throw error
      })
  }
}

//获取当前城市名称
export const getCity = (opt) => {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      fetch(apis.getCity+opt)
        .then((response) => {
          response.json().then(res=>{
            if(res.status == 0){
              const { city } = res.result.addressComponent
              let cityName = city.substring(0,city.length-1)
              dispatch(getCitySuccess(cityName))
              dispatch(getCityData(cityName))
              resolve(cityName)
            }
          }).catch()
        })
        .catch((error) => {
          Toast.showShortBottom(error.message)
          dispatch(hideLoading())
        })
      }
    )
  }
}

export const sendCode = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.sendCode, 'post', opt)
      .then((res) => {
        console.log(res)
        dispatch(sendCodeOk())
        // Toast.showShortBottom('发送成功')
        dispatch(hideLoading())
      })
      .catch((error) => {
        console.log(error.message)
        Toast.showShortBottom(error.message)
        dispatch(hideLoading())
        throw error
      })
  }
}

// 用户登录
export const login = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.login, 'post', opt).then((res) => {
      console.log('login res:',res)
      dispatch(loginSuccess(res))
      dispatch(queryDeviceList(res))
      dispatch(hideLoading())
      store.save('user', res)
      store.save('phone',res.phone)
    }).catch((error) => {
      console.log(error.message)
      Toast.showShortBottom(error.message || '网络异常,请重试')
      dispatch(hideLoading())
      throw error
    })
  }
}
export const autoLogin = () => {
  return (dispatch) => {
    return http.request(apis.autoLogin, 'post').then((res) => {
      dispatch(loginSuccess(res))
      store.update('user', res)
      store.save('phone',res.phone)
      dispatch(queryDeviceList(res))
      return 'autoLogin success'
    }).catch((error) => {
      console.log(error.message)
      NavigatorService.navigate('Login')
      throw(error.message)
    })
  }
}

export const modifyPassword = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.modifyPassword, 'post', opt)
      .then((res) => {
        dispatch(hideLoading())
        dispatch(logOut())
      })
      .catch((error) => {
        Toast.showShortBottom(error.message || '网络异常,请重试')
        dispatch(hideLoading())
        throw error.message
      })
  }
}
// 用户忘记密码
export const forgetPassword = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.forgetPassword, 'post', opt)
      .then((res) => {
        console.log(res)
        dispatch(hideLoading())
        Toast.showShortBottom(res)
        NavigatorService.goBack()
      })
      .catch((error) => {
        Toast.showShortBottom(error.message || '网络异常,请重试')
        dispatch(hideLoading())
      })
  }
}
// 用户修改手机号码
export const modifyPhone = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.modifyPhone, 'post', opt)
      .then((res) => {
        dispatch(hideLoading())
        dispatch(loginSuccess(res))//更新用户信息
        dispatch(logOut())
        Toast.showShortBottom('手机号修改成功，请使用新手机号登录')
      })
      .catch((error) => {
        console.log(error.message)
        Toast.showShortBottom(error.message || '网络异常,请重试')
        dispatch(hideLoading())
        throw error
      })
  }
}
// 用户修改个人信息
export const modifyPersonInfo = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.modifyPersonInfo, 'post', opt)
      .then((res) => {
        dispatch(hideLoading())
        dispatch(loginSuccess(res))//更新用户信息
        // Toast.showShortBottom('修改成功')
        return res
      })
      .catch((error) => {
        dispatch(hideLoading())
        console.log(error.message)
        Toast.showShortBottom(error.message || '网络异常,请重试')
      })
  }
}
export const logOut = (opt) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.logOut, 'post')
      .then((res) => {
        dispatch(hideLoading())
        dispatch(logOutSuccess())
        store.delete('user')
      })
      .catch((error) => {
        dispatch(hideLoading())
        console.log(error.message)
        Toast.showShortBottom(error.message || '网络异常,请重试')
      })
  }
}

export const queryDeviceList = (opt) => {
  const { uid = '' } = opt
  return (dispatch) => {
    return http.request(apis.queryDeviceList, 'get',{uid})
      .then((res) => {
        console.log(res)
        dispatch(queryDeviceListSuccess(res))
        if(res.length === 0 ){
          //没有已绑定的设备
          // NavigatorService.navigate('BindDevice')
        }
        return res
      })
      .catch((error) => {
        Toast.showShortBottom(error.message || '网络异常,请重试')
      })
  }
}

export const addDevice = (body) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.addDevice, 'post',body)
      .then((res) => {
        dispatch(hideLoading())
        dispatch(queryDeviceList(body))
        return res
      })
      .catch((error) => {
        // Toast.showShortBottom(error.message || '网络异常,请重试')
        dispatch(hideLoading())
        throw error
      })
  }
}
export const unBindDevice = (body) => {
  console.log(body)
  return (dispatch) => {
    // dispatch(showLoading())
    return http.request(apis.unBindDevice, 'post',body)
      .then((res) => {
        dispatch(hideLoading())
        dispatch(queryDeviceList(body))
        return res
      })
      .catch((error) => {
        console.log(error.message)
        dispatch(hideLoading())
        throw error
      })
  }
}
export const getAboutUs = () => {
  return (dispatch) => {
    return http.request(apis.getAboutUs, 'get')
      .then((res) => {
        dispatch(getAboutUsSuccess(res))
        return res
      })
      .catch((error) => {
        console.log(error.message)
        throw error
      })
  }
}
export const getHistory = (body) => {
  console.log(body)
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.getHistory, 'get',body)
      .then((res) => {
        dispatch(hideLoading())
        console.log({res})
        dispatch(getHistorySuccess(res.items))
        return res
      })
      .catch((error) => {
        console.log(error.message)
        dispatch(hideLoading())
        throw error
      })
  }
}
export const chooseDevice = mac => {
  store.save('defaultDevice',mac)
  return {
    type: 'CHOOSE_DEVICE',
    payload: mac
  }
}
export const getHistorySuccess = res => {
  return {
    type: 'GET_HISTORY_SUCCESS',
    payload: res
  }
}
export const getMessagesSuccess = res => {
  return {
    type: 'GET_MESSAGES_SUCCESS',
    payload: res
  }
}
export const getCitySuccess = city => {
  return {
    type: 'GET_CITY_SUCCESS',
    payload: city
  }
}
export const getAboutUsSuccess = res => {
  return {
    type: 'GET_ABOUT_US_SUCCESS',
    payload: res
  }
}
export const getMessages = (body) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.getMessages, 'get',body)
      .then((res) => {
        dispatch(hideLoading())
        console.log({res})
        dispatch(getMessagesSuccess(res.items))
        return res
      })
      .catch((error) => {
        console.log(error.message)
        dispatch(hideLoading())
        throw error
      })
  }
}

export const deleteMessages = (body) => {
  return (dispatch) => {
    dispatch(showLoading())
    return http.request(apis.deleteMessages, 'post',body)
      .then((res) => {
        dispatch(hideLoading())
        console.log({res})
        return res
      })
      .catch((error) => {
        console.log(error.message)
        dispatch(hideLoading())
        throw error
      })
  }
}

export const getCityData = (city) => {
  console.log('wwwww')
  return (dispatch) => {
    return http.request(apis.getCityData, 'get',{city})
      .then((res) => {
        if(res) dispatch(getCityDataSuccess(res))
      })
      .catch((error) => {
        console.log(city,error.message)
        // throw error
      })
  }
}

export const getDeviceStatus = (mac) => {
  return (dispatch) => {
    return http.request(apis.getDeviceStatus, 'get',{mac})
      .then((res) => {
        if(res){
          dispatch(getDeviceStatusSuccess(res))
        }else{
          dispatch(getDeviceStatusSuccess({}))
        }
        return res
      })
      .catch((error) => {
        console.log(error.message)
        throw error
      })
  }
}
export const getNowData = (mac) => {
  return (dispatch) => {
    return http.request(apis.getNowData, 'get',{mac})
      .then((res) => {
        if(res){
          dispatch(getNowDataSuccess(res))
        }else{
          dispatch(getNowDataSuccess({}))
        }
        return res
      })
      .catch((error) => {
        console.log(error.message)
        throw error
      })
  }
}
export const getWeekData = (mac) => {
  return (dispatch) => {
    return http.request(apis.getWeekData, 'get',{mac})
      .then((res) => {
        if(res){
          dispatch(getWeekDataSuccess(res))
        }
        return res
      })
      .catch((error) => {
        console.log(error.message)
        throw error
      })
  }
}
export const getOrderParam = (mac) => {
  return (dispatch) => {
    return http.request(apis.getOrderParam, 'get',{mac})
      .then((res) => {
        if(res){
          dispatch(getOrderParamSuccess(res))
        }
        return res
      })
      .catch((error) => {
        console.log(error.message)
        throw error
      })
  }
}
export const setDeviceName = (body) => {
  return (dispatch) => {
    return http.request(apis.setDeviceName, 'post',body)
      .then((res) => {
        dispatch(queryDeviceList(body))
        return res
      })
      .catch((error) => {
        console.log(error.message)
        throw error
      })
  }
}
let timeOrder
export const setOrderParam = (body,time = 5000) => {
  return (dispatch) => {
    return http.request(apis.setOrderParam, 'post',body)
      .then((res) => {
        timeOrder && clearTimeout(timeOrder)
        timeOrder = setTimeout(()=>dispatch(getOrderParam),time)
        return res
      })
      .catch((error) => {
        console.log(error.message)
        throw error
      })
  }
}
export const setTime = (mac) => {
  return (dispatch) => {
    return http.request(apis.setTime+'?mac='+mac, 'post')
      .then((res) => {
        return res
      })
      .catch((error) => {
        throw error
      })
  }
}

export const isDrawOpen = open => {
  return {
    type: 'IS_DRAW_OPEN',
    payload: open
  }
}
export const saveVersion = version => {
  return {
    type: 'SAVE_VERSION',
    payload: version
  }
}

export const getCityDataSuccess = data => {
  return {
    type: 'GET_CITY_DATA_SUCCESS',
    payload: data
  }
}

export const getNowDataSuccess = data => {
  return {
    type: 'GET_NOW_DATA_SUCCESS',
    payload: data
  }
}

export const getWeekDataSuccess = data => {
  return {
    type: 'GET_WEEK_DATA_SUCCESS',
    payload: data
  }
}

export const getDeviceStatusSuccess = data => {
  return {
    type: 'GET_DEVICE_STATUS_SUCCESS',
    payload: data
  }
}

export const getOrderParamSuccess = data => {
  return {
    type: 'GET_ORDER_PARAM_SUCCESS',
    payload: data
  }
}



