// 公共地址
// const hostBase = 'http://192.168.10.161:8088'
// const hostBase = 'http://m.sensicfusion.com/melt'
// const hostBase = 'http://192.168.10.159:8088'
const hostBase = 'http://172.26.2.30:8088'

// 添加用户信息
export const getUserMsg = hostBase + '/v1/app/user/register'
// 用户登录接口
export const login = hostBase + '/v1/app/user/applogin'
// 用户自动登录接口
export const autoLogin = hostBase + '/v1/app/user/appAutoLogin'
// 用户忘记密码
export const forgetPassword = hostBase + '/v1/app/user/forgetpassword'
// 用户修改密码
export const modifyPassword = hostBase + '/v1/app/user/modifypassword'
// 用户修改手机号码
export const modifyPhone = hostBase + '/v1/app/user/modifyphone'
// 用户修改个人信息
export const modifyPersonInfo = hostBase + '/v1/app/user/modifyuser'
// 发送手机验证码
export const sendCode = hostBase + '/v1/app/user/sendcode'
// 退出登录
export const logOut = hostBase + '/v1/app/user/logout'
export const queryDeviceList = hostBase + '/v1/app/device/queryDeviceList'
export const addDevice = hostBase + '/v1/app/device/adddevice'
export const unBindDevice = hostBase + '/v1/app/device/deluserdevice'
export const getHistory = hostBase + '/v1/app/device/findlisthistory'
export const getAboutUs = hostBase + '/v1/app/aboutUs/info'
export const getMessages = hostBase + '/v1/app/message/query'
export const deleteMessages = hostBase + '/v1/app/message/del'
export const getCityData = hostBase + '/v1/app/index/data'
export const getDeviceStatus = hostBase + '/v1/app/device/getStatusParam'
export const getNowData = hostBase + '/v1/app/device/realTimeData'
export const getWeekData = hostBase + '/v1/app/device/weekData'
export const getOrderParam = hostBase + '/v1/app/device/getOrderParam'
export const setDeviceName = hostBase + '/v1/app/device/updateDeviceName'
export const setOrderParam = hostBase + '/v1/app/device/setOrderParam'
export const setTime = hostBase + '/v1/app/device/setTime'



export const getCity = 'https://api.map.baidu.com/geocoder/v2/?output=json&ak=rTigWhK2f5ECTl1BRKbfKzn19zSlVzZV'
