import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import pxToDp from '../utils/pxToDp'

class HomeChartTitle extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      AQI: true,
      PM2_5: false,
      PM10: false
    }
  }

  selectLeft = () => {
    this.setState({
      AQI: true,
      PM2_5: false,
      PM10: false
    })
  }

  selectMiddle = () => {
    this.setState({
      PM2_5: true,
      AQI: false,
      PM10: false
    })
  }

  selectRight = () => {
    this.setState({
      PM10: true,
      AQI: false,
      PM2_5: false
    })
  }

  render () {
    const { navigation } = this.props
    let { AQI, PM2_5, PM10 } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <TouchableOpacity
            style={styles.titleWidth}
            onPress={this.selectLeft}
          >
            <Text style={AQI === true ? styles.titleTextBig : styles.titleText}>AQI</Text>
            {AQI === true ? <View style={styles.titleBottomLine} /> : null }
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.titleWidth}
            onPress={this.selectMiddle}
          >
            <Text style={PM2_5 === true ? styles.titleTextBig : styles.titleText}>PM2.5</Text>
            {PM2_5 === true ? <View style={styles.titleBottomLine} /> : null }
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.titleWidth}
            onPress={this.selectRight}
          >
            <Text style={PM10 === true ? styles.titleTextBig : styles.titleText}>PM10</Text>
            {PM10 === true ? <View style={styles.titleBottomLine} /> : null }
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: pxToDp(80),
    marginTop: pxToDp(30)
  },
  title: {
    flexDirection: 'row'
  },
  titleWidth: {
    flex: 1,
    alignItems: 'center'
  },
  titleText: {
    textAlign: 'center',
    color: '#999'
  },
  titleTextBig: {
    textAlign: 'center',
    color: '#333'
  },
  titleBottomLine: {
    width: pxToDp(50),
    height: pxToDp(8),
    backgroundColor: '#3FABFC',
    borderRadius: pxToDp(4),
    marginTop: pxToDp(8)
  },
  details: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(50),
    marginVertical: pxToDp(20)
  },
  deLeft: {
    width: pxToDp(86)
  },
  deMiddle: {
    flex: 1
  },
  deRight: {
    width: pxToDp(120)
  },
  deRightAQI: {
    width: pxToDp(46)
  }
})

export default HomeChartTitle
