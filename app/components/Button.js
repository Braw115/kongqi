import React, { Component } from 'react'
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native'
import pxToDp from '../utils/pxToDp'

class Button extends Component {
  render () {
    const {
      accessibilityLabel,
      color = '#3FABFC',
      onPress,
      title,
      disabled,
      outline,
      buttonStyle,
      textStyle,
      ...attributes
    } = this.props
    const buttonStyles = [styles.button]
    const textStyles = [styles.text, textStyle]

    if (outline) {
      buttonStyles.push({
        backgroundColor: '#FFF',
        borderWidth: 2 * StyleSheet.hairlineWidth,
        borderColor: color
      })
      textStyles.push({color: color})
    } else {
      buttonStyles.push({
        backgroundColor: color
      })
    }
    const accessibilityTraits = ['button']
    if (disabled) {
      buttonStyles.push(styles.buttonDisabled)
      textStyles.push(styles.textDisabled)
      accessibilityTraits.push('disabled')
    }
    return (
      <TouchableOpacity
        accessibilityComponentType='button'
        accessibilityLabel={accessibilityLabel}
        accessibilityTraits={accessibilityTraits}
        disabled={disabled}
        onPress={onPress}
        {...attributes}
      >
        <View style={[buttonStyles, buttonStyle]}>
          <Text style={textStyles} disabled={disabled}>{title}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#3FABFC',
    justifyContent: 'center',
    height: pxToDp(100),
    borderRadius: pxToDp(50),
    overflow: 'hidden'
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: pxToDp(32),
    paddingHorizontal: pxToDp(14),
    fontWeight: '500'
  },
  buttonDisabled: {
    backgroundColor: '#d8d8d8'
  },
  textDisabled: {
    color: '#fff'
  }
})

export default Button
