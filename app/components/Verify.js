import React, { Component } from 'react'
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native'
import pxToDp from '../utils/pxToDp'

class Verify extends Component {
  render () {
    const {
      verify,
      verifyText,
      onPress
    } = this.props
    return (
      <TouchableOpacity disabled={!verify} style={verify ? styles.codeOn : styles.codeOff} onPress={onPress} >
        <Text style={verify ? styles.codeTextOn : styles.codeTextOff}>{verifyText}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#3FABFC',
    justifyContent: 'center',
    height: pxToDp(100),
    borderRadius: pxToDp(50),
    overflow: 'hidden'
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: pxToDp(32),
    paddingHorizontal: pxToDp(14),
    fontWeight: '500'
  },
  buttonDisabled: {
    backgroundColor: '#d8d8d8'
  },
  textDisabled: {
    color: '#fff'
  }
})

export default Verify
