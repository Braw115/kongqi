import React, { Component } from 'react'
import { Modal, StyleSheet, SafeAreaView, View, StatusBar } from 'react-native'
import Spinner from 'react-native-spinkit'

export default class Loading extends Component {
  constructor (props) {
    super(props)
  }
  componentDidMount () {
    StatusBar.setBarStyle('dark-content', true)
    // StatusBar.setBackgroundColor('#eee')
  }
  render () {
    const {show} = this.props
    return (
      <Modal transparent style={styles.modal} visible={show} onRequestClose={() => {}} animationType={'fade'} >
        <View style={styles.container}>
          <SafeAreaView>
            <StatusBar barStyle={'dark-content'} animated />
            <Spinner isVisible={show} size={70} type={'Circle'} color={'rgb(176,226,255)'} />
          </SafeAreaView>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  modal: {
    marginTop:50,
    backgroundColor: '#F5FCFF',
    marginHorizontal: 0,
    marginBottom: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden'
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
})