import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import DummyNavButton from './DummyNavButton'
import pxToDp from '../../utils/pxToDp'

const Title = (props) => {
  const {
    text,
    onPress,
    ...attributes
  } = props
  const navTitleStyle = [styles.navTitle]
  const titleStyle = [styles.title]
  return (
    // 右边文字按钮
    onPress && typeof onPress === 'function' ? (
      [
        <DummyNavButton key={'dummy'} />,
        <TouchableOpacity key={'rightText'} activeOpacity={0.6} style={styles.navContainer} onPress={onPress}>
          <Text style={navTitleStyle} numberOfLines={1} {...attributes}>
            {text}
          </Text>
        </TouchableOpacity>
      ]
    ) : (
    // 中间标题
      <View style={styles.container}>
        <Text style={titleStyle} numberOfLines={1} {...attributes}>
          {text}
        </Text>
      </View>
    )
  )
}
const styles = StyleSheet.create({
  navContainer: {
    height: '100%',
    position: 'absolute',
    right: 0,
    paddingHorizontal: pxToDp(30),
    justifyContent: 'center'
  },
  container: {
    flex: 1
  },
  title: {
    fontSize: pxToDp(36),
    color: '#333333',
    textAlign: 'center'
  },
  navTitle: {
    fontSize: pxToDp(32),
    color: '#FFF'
  }
})
export default Title
