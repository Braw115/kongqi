import React from 'react'
import { StyleSheet, View } from 'react-native'
import pxToDp from '../../utils/pxToDp'

const DummyNavButton = () => (
  <View style={styles.dummyNavBtn} />
)

const styles = StyleSheet.create({
  dummyNavBtn: {
    height: pxToDp(88),
    width: pxToDp(88)
  }
})

export default DummyNavButton
