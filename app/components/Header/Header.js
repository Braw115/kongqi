import React from 'react'
import { View, StyleSheet, Platform, StatusBar } from 'react-native'
import Title from './Title'
import NavButton from './NavButton'
import DummyNavButton from './DummyNavButton'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import pxToDp from '../../utils/pxToDp'

// 生成Header的左中右三个子组件
const generateChild = (Comp, type, value) => {
  if (React.isValidElement(Comp)) {
    // 如果传入的是React元素,加上key后返回
    return (
      <Comp.type {...Comp.props} key={type} />
    )
  } else if (Comp && typeof Comp === 'object' && Comp.value) {
    // 如果传入的是对象,对象的属性有 type,value,onPress
    // 当type为text时,value的值为字符串
    // 当type为icon时,value的值为require('图标路径')
    // onPress为点击的回调函数
    return Comp.type === 'text'
      ? <Title key={type} text={Comp.value} onPress={Comp.onPress} />
      : <NavButton key={type} icon={Comp.value} onPress={Comp.onPress} />
  } else if (type === 'left' && typeof value === 'function') {
    // 如果leftComponent没有传值时,value为传入的back,即返回按钮的回调函数
    return (
      <NavButton
        key={type}
        icon={require('../../images/back.png')}
        onPress={value} />
    )
  } else if (type === 'center') {
    // 如果centerComponent没有传值时,value为传入的title,即标题
    return typeof value === 'string' ? <Title key={type} text={value} /> : null
  } else {
    // 以上条件都不符合时，返回占位元素
    return Comp === null ? null : <DummyNavButton key={type} />
  }
}

const populateChildren = (propChildren) => {
  const childrenArray = []
  const leftComponent = generateChild(propChildren.leftComponent, 'left', propChildren.back)
  const centerComponent = generateChild(propChildren.centerComponent, 'center', propChildren.title)
  const rightComponent = generateChild(propChildren.rightComponent, 'right')

  childrenArray.push(
    leftComponent,
    centerComponent,
    rightComponent
  )
  return childrenArray
}

const Header = (props) => {
  const {
    back, // 返回按钮的回调函数,使用默认的返回图标
    title, // Header标题
    leftComponent, // 左组件
    centerComponent, // 中间组件
    rightComponent, // 右组件
    innerContainerStyles, // Header组件样式
    outerContainerStyles, // Header容器组件样式
    ...attributes // 剩余的props
  } = props
  // StatusBar.setBackgroundColor('transparent')
  const outerContainer = [styles.outerContainer]
  let propChildren = populateChildren({ leftComponent, centerComponent, rightComponent, back, title })
  return (
    <View style={[outerContainer, outerContainerStyles]} {...attributes}>
      <View style={[styles.innerContainer, innerContainerStyles]}>
        {propChildren}
      </View>
    </View>
  )
}

// const paddingTop = Platform.OS === 'android' ? 0 : getStatusBarHeight()
const paddingTop = getStatusBarHeight()
const styles = StyleSheet.create({
  outerContainer: {
    backgroundColor: '#fff',
    paddingTop: paddingTop,
    height: pxToDp(88) + paddingTop
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth:1,
    borderBottomColor:'#D8D8D8'
  }
})

export default Header
