import React from 'react'
import { Image, StyleSheet, TouchableOpacity } from 'react-native'
import pxToDp from '../../utils/pxToDp'

const NavButton = (props) => {
  const {
    icon,
    ...attributes
  } = props

  return (
    <TouchableOpacity activeOpacity={0.6} style={styles.navContainer} {...attributes}>
      <Image style={styles.navIcon} source={icon} />
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  navContainer: {
    width: pxToDp(88),
    height: pxToDp(88),
    alignItems: 'center',
    justifyContent: 'center'
  },
  navIcon: {
    width: pxToDp(20),
    height: pxToDp(36),
    resizeMode: 'contain'
  }
})
export default NavButton
