import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text, Modal, TextInput, StatusBar } from 'react-native'
import pxToDp from '../utils/pxToDp'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import {check} from '../utils/function'
import store from 'react-native-simple-store'
import Toast from 'react-native-toast'

class NameModal extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      uid: '',
      oldName: ''
    }
  }

  componentDidMount () {
    store.get('user').then(res => {
      this.setState({uid: res.uid, oldName: res.name})
    }).catch((err) => {})
  }

  onSubmit = (parent) => {
    const { name, oldName } = this.state
    const {actions, publicReducer} = this.props
    const {uid} = publicReducer.myInfo
    if (check.Name(name)) {
      if(name === oldName){
        Toast.showShortBottom('请输入新昵称')
        return false
      }
      let body = { uid, name }
      actions.modifyPersonInfo(body).then(()=>{
        parent.setState({ tipVisible: false })
      }).catch()
    } else {
      return false
    }

  }

  render () {
    let { parent } = this.props
    return (
      <Modal
        animationType={'fade'}
        transparent
        visible={parent.state.tipVisible}
        onRequestClose={() => {
          parent.setState({ tipVisible: false })
        }}>
        <StatusBar barStyle={'light-content'} />
        <View style={styles.modalStyle}>
          <View style={styles.modalFrame}>
            <View style={styles.modalMiddle}>
              <Text style={styles.name}>昵称</Text>
              <View style={styles.nameNew}>
                <TextInput
                  style={styles.inpStyle}
                  placeholder='请输入新昵称'
                  autoFocus
                  underlineColorAndroid='transparent'
                  placeholderTextColor='#999999'
                  onChangeText={(text) => this.setState({ name: text })}
                />
              </View>
            </View>
            <View style={styles.nameConfirm}>
              <TouchableOpacity
                style={styles.touConfirm}
                onPress={() => { parent.setState({ tipVisible: false }) }}
              >
                <Text style={{fontSize: pxToDp(34), color: '#999'}}>取消</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.touConfirm}
                onPress={() => {this.onSubmit(parent)}}
              >
                <Text style={{fontSize: pxToDp(34), color: '#3FABFC'}} >确定</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  modalStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.4)'
  },
  modalFrame: {
    width: pxToDp(622),
    height: pxToDp(404),
    borderRadius: pxToDp(5),
    backgroundColor: '#fff',
    marginTop: pxToDp(100)
  },
  modalMiddle: {
    marginTop: '5%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  nameNew: {
    marginTop: '8%',
    width: '80%'
  },
  inpStyle: {
    width: '100%',
    fontSize: pxToDp(32),
    color: '#999',
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8'
  },
  nameConfirm: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: '10%'
  },
  touConfirm: {
    // marginRight: pxToDp(40),
    paddingHorizontal: pxToDp(20)
  },
  name: {
    fontSize: pxToDp(32),
    color: '#333'
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NameModal)
