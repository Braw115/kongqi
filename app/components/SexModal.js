import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text, Modal, Image, StatusBar } from 'react-native'
import pxToDp from '../utils/pxToDp'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import store from 'react-native-simple-store'

class SexModal extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      selected: props.sex
    }
  }
  componentDidMount () {
    store.get('user').then(res => {
      this.setState({ uid: res.uid })
    }).catch(( err )=> {})
  }
  onSubmit = (parent) => {
    const { actions } = this.props
    const { selected,uid } = this.state
    let sex = selected ? 1 : 0
    if(selected != this.props.sex){
      let body = { uid, sex }
      actions.modifyPersonInfo(body).then(()=>{
        parent.setState({ sexVisible: false })
      }).catch()
    }else{
      parent.setState({ sexVisible: false })
    }
  }

  radioSex = (sex) => {
    this.setState({ selected: !!sex })
  }


  render () {
    let { parent } = this.props

    return (
      <Modal
        animationType={'fade'}
        transparent
        visible={parent.state.sexVisible}
        onRequestClose={() => {
          parent.setState({ sexVisible: false })
        }}>
        <StatusBar barStyle={'light-content'} />
        <View style={styles.modalStyle}>
          <View style={styles.modalFrame}>
            <Text style={styles.topTitle}>性别</Text>
            <View style={styles.sexLine}>
              <TouchableOpacity
                style={styles.touSelected}
                onPress={() => this.radioSex(0)}
              >
                {
                  !this.state.selected
                    ? <Image style={styles.deng} source={require('../images/deng.png')} />
                    : <View style={styles.selectedSexBox} />
                }
                <Text style={styles.sexText}>男</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.touSelected}
                onPress={() => this.radioSex(1)}
              >
                {
                  this.state.selected
                    ? <Image style={styles.deng} source={require('../images/deng.png')} />
                    : <View style={styles.selectedSexBox} />
                }
                <Text style={styles.sexText}>女</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.nameConfirm}>
              <TouchableOpacity
                style={styles.touConfirm}
                onPress={() => { parent.setState({ sexVisible: false }) }}
              >
                <Text style={{fontSize: pxToDp(34), color: '#999'}}>取消</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.touConfirm}
                onPress={() => { this.onSubmit(parent) }}
              >
                <Text style={{fontSize: pxToDp(34), color: '#3FABFC'}} >确定</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  modalStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.4)'
  },
  modalFrame: {
    width: pxToDp(622),
    height: pxToDp(404),
    borderRadius: pxToDp(5),
    backgroundColor: '#fff'
  },
  topTitle: {
    textAlign: 'center',
    paddingTop: pxToDp(36),
    paddingBottom: pxToDp(36),
    fontSize: pxToDp(32),
    color: '#333'
  },
  sexLine: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  touSelected: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(80),
    alignItems: 'center'
  },
  selectedSexBox: {
    width: pxToDp(40),
    height: pxToDp(40),
    borderRadius: pxToDp(20),
    borderWidth: 1,
    borderColor: '#999'
  },
  nameConfirm: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: pxToDp(120)
  },
  touConfirm: {
    marginHorizontal: pxToDp(80)
  },
  deng: {
    width: pxToDp(40),
    height: pxToDp(40)
  },
  sexText: {
    marginLeft: pxToDp(10),
    fontSize: pxToDp(32)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SexModal)
