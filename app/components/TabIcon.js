import { Image, View, Text, StatusBar, TouchableOpacity } from 'react-native'
import React from 'react'
import  pxToDp  from '../utils/pxToDp'
const TabIcon = (props) => {
  return (
    <View style={{ alignItems: 'center', height: pxToDp(97)}} >
      <Image
        style={[{height: pxToDp(40), width: pxToDp(40),marginTop:pxToDp(12), marginBottom: pxToDp(3)},props.style]}
        resizeMode={'contain'}
        source={props.focused ? props.active : props.inActive} />
      <Text style={{fontSize: pxToDp(24), color: props.focused ? '#24B5FF' : '#8E8E8E'}}>{props.title}</Text>
    </View>)
}

export default TabIcon
