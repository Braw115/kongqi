import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text, Modal, TextInput } from 'react-native'
import pxToDp from '../utils/pxToDp'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import * as checkActions from '../actions/checkAction'

import store from 'react-native-simple-store'

class NickModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      nick: '',
      uid: ''
    }
  }

  componentDidMount() {
    store.get('user').then(res => {
      this.setState({ uid: res.uid })
    })
  }

  onSubmit = (parent) => {
    console.log(parent)
    // const { actions, checkActions } = this.props
    // const { nick, uid } = this.state

    // var fin = {
    //   'nick': nick,
    //   'uid': uid
    // }
    // console.log(JSON.stringify(fin))
    // var body = JSON.stringify(fin)

    // actions.modifyPersonInfo(body)

    parent.setState({ tipVisible: false })
  }

  render() {
    let { parent } = this.props

    return (
      <Modal
        animationType={'fade'}
        transparent
        visible={parent.state.tipVisible}
        onRequestClose={() => {
          parent.setState({ tipVisible: false })
        }}>
        <View style={styles.modalStyle}>
          <View style={styles.modalFrame}>
            <View style={styles.modalMiddle}>
              <Text>昵称</Text>
              <View style={styles.nickNew}>
                <TextInput
                  style={styles.inpStyle}
                  placeholder='请输入新昵称'
                  autoFocus
                  placeholderTextColor='#999999'
                  onChangeText={(text) => this.setState({ nick: text })}
                />
              </View>
            </View>
            <View style={styles.nickConfirm}>
              <TouchableOpacity
                style={styles.touConfirm}
                onPress={() => { parent.setState({ tipVisible: false }) }}
              >
                <Text>取消</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.touConfirm}
                // onPress={() => { this.onSubmit(parent) }}
                onPress={() => { parent.setState({ tipVisible: false }) }}
              >
                <Text>确定</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  modalStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.4)'
  },
  modalFrame: {
    width: '81%',
    height: '46%',
    borderRadius: pxToDp(5),
    backgroundColor: '#fff'
  },
  modalMiddle: {
    marginTop: '5%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  nickNew: {
    marginTop: '8%',
    width: '80%'
  },
  inpStyle: {
    width: '100%'
  },
  nickConfirm: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: '10%'
  },
  touConfirm: {
    // marginRight: pxToDp(40),
    paddingHorizontal: pxToDp(20)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch),
    checkActions: bindActionCreators({ ...checkActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NickModal)
