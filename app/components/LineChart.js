import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { VictoryLine, VictoryScatter, VictoryChart, VictoryTheme } from 'victory-native'
import pxToDp from '../utils/pxToDp'

const data = [
  { x: '06 - 29', y: 125 },
  { x: '06 - 30', y: 220 },
  { x: '07 - 01', y: 110 },
  { x: '07 - 02', y: 300 },
  { x: '07 - 03', y: 500 }
]

class LineChart extends React.Component {
  render () {
    return (
      <View>
        <Text style={styles.title}>(ug/m3)</Text>
        <VictoryChart height={pxToDp(420)} maxDomain={{y: 500}} minDomain={{y: 0}} theme={VictoryTheme.material} >
          <VictoryLine
            data={data}
            style={{ data: { stroke: '#3FABFC', strokeWidth: pxToDp(3) } }}
            animate={{
              duration: 3000
            }}
          />
          <VictoryScatter data={data}
            size={pxToDp(5)}
            style={{ data: { fill: '#fff', stroke: '#FF5368', strokeWidth: pxToDp(4) }, parent: { border: '1px solid #ccc' } }}
          />
        </VictoryChart>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: pxToDp(-40),
    marginHorizontal: pxToDp(30)
  }
})

export default LineChart
