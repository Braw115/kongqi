import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, Platform,Linking,Alert } from 'react-native'
import pxToDp from '../utils/pxToDp'
import Toast from 'react-native-toast'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'
import {tool} from '../utils/function'
import _updateConfig from '../../update.json'
import { isFirstTime, isRolledBack, packageVersion, currentVersion, checkUpdate, downloadUpdate, switchVersion, switchVersionLater, markSuccess } from 'react-native-update'
const {appKey} = _updateConfig[Platform.OS]
class Aside extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      phone: ''
    }
  }

  vertip = () => {
    // let version = this.props.publicReducer.version
    // let newVersion = '1.00.02'
    // for(let i=0;i<version.length;i++){
    //   if( version[i] != newVersion ){
    //     if( version[i] < newVersion[i] ){
    //       this.openMarket()
    //       return
    //     }
    //   }
    // }
    this.checkUpdate()
    // Toast.showShortCenter('当前是最新版本')
  }
  componentWillMount(){
    if (isFirstTime) {
      Alert.alert('提示', '这是当前版本第一次启动,是否要模拟启动失败?失败将回滚到上一版本', [
        {text: '是', onPress: ()=>{throw new Error('模拟启动失败,请重启应用')}},
        {text: '否', onPress: ()=>{markSuccess()}},
      ]);
    } else if (isRolledBack) {
      Alert.alert('提示', '刚刚更新失败了,版本被回滚.');
    }
  }
  doUpdate = info => {
    downloadUpdate(info).then(hash => {
      Alert.alert('提示', '下载完毕,是否重启应用?', [
        {text: '是', onPress: ()=>{switchVersion(hash);}},
        {text: '否',},
        {text: '下次启动时', onPress: ()=>{switchVersionLater(hash);}},
      ]);
    }).catch(err => {
      Alert.alert('提示', '更新失败.');
    });
  };
  checkUpdate = () => {
    checkUpdate(appKey).then(info => {
      console.log(info)
      if (info.expired) {
        Alert.alert('提示', '您的应用版本已更新,请前往应用商店下载新的版本', [
          {text: '确定', onPress: ()=>{info.downloadUrl && Linking.openURL(info.downloadUrl)}},
        ]);
      } else if (info.upToDate) {
        Alert.alert('提示', '您的应用版本已是最新.');
      } else {
        Alert.alert('提示', '检查到新的版本'+info.name+',是否下载?\n'+ info.description, [
          {text: '是', onPress: ()=>{this.doUpdate(info)}},
          {text: '否',},
        ]);
      }
    }).catch(err => {
      Alert.alert('提示', '更新失败.');
    });
  };
  openMarket = () => {
    // const url = Platform.OS == 'android' ? 'http://play.google.com/store/apps/details?id=com.sensic.aqdr': 'itms-apps://'
    // Linking.canOpenURL(url)
    //   .then((supported) => {
    //     if (!supported) {
    //       alert("Can't handle url: " + url);
    //     } else {
    //       return Linking.openURL(url);
    //     }
    //   })
    //   .catch((err) => alert('err'))
  }

  jump = (name) => {
    const { navigation, actions } = this.props.parent
    navigation.navigate(name)
    // actions.isDrawOpen(true)
  }


  render () {
    const { navigation } = this.props.parent
    const { auth, myInfo, version } = this.props.publicReducer
    const newPhone = tool.hidePhone(myInfo.phone)
    return (
      <View style={styles.container}>
        <View >
          <TouchableOpacity
            style={styles.TopMenu}
            onPress={() => { auth ? this.jump('PersonInfo') : this.jump('Login') }}
          >
            <View style={styles.smallPicPad}>
              <Image style={styles.login} source={require('../images/personalCenter.png')} />
            </View>
            <Text style={styles.asideTopTitle}>{auth?newPhone:"登录/注册"}</Text>
          </TouchableOpacity>
          <Text style={styles.name} >{myInfo.name}</Text>
          <View style={styles.line}></View>
          <TouchableOpacity
            style={[styles.list, { marginTop: pxToDp(50) }]}
            onPress={() => { this.jump('Message') }}
          >
            <View style={styles.smallPicPad}>
              <Image style={styles.smallPic} source={require('../images/myMessage.png')} />
            </View>
            <Text style={styles.asideText}>我的消息</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.list, { marginTop: pxToDp(50) }]}
            onPress={() => { this.jump('BindDevice') }}
          >
            <View style={styles.smallPicPad}>
              <Image style={styles.smallPic} source={require('../images/bindDevice.png')} />
            </View>
            <Text style={styles.asideText}>设备绑定</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.list}
            onPress={() => { this.jump('HistoryRecord') }}
          >
            <View style={styles.smallPicPad}>
              <Image style={styles.smallPic} source={require('../images/history.png')} />
            </View>
            <Text style={styles.asideText}>历史记录</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.list}
            onPress={() => { this.jump('AboutUs') }}
          >
            <View style={styles.smallPicPad}>
              <Image style={styles.smallPic} source={require('../images/aboutUs.png')} />
            </View>
            <Text style={styles.asideText}>关于我们</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.list}
            onPress={this.vertip}
          >
            <View style={styles.smallPicPad}>
              <Image style={styles.smallPic} source={require('../images/version.png')} />
            </View>
            <View style={styles.version}>
              <Text style={styles.asideText}>版本检测</Text>
              <Text style={styles.versionNum}>v{version}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  TopMenu: {
    flexDirection: 'row',
    marginTop: pxToDp(120),
    alignItems: 'center'
  },
  list: {
    flexDirection: 'row',
    marginTop: pxToDp(45),
    alignItems: 'center'
  },
  login: {
    width: pxToDp(44),
    height: pxToDp(44)
  },
  smallPic: {
    width: pxToDp(50),
    height: pxToDp(50)
  },
  smallPicPad: {
    marginLeft: pxToDp(50),
    marginRight: pxToDp(34)
  },
  version: {
    width: '68%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  asideTopTitle: {
    fontSize: pxToDp(36),
    color: '#333'
  },
  asideText: {
    fontSize: pxToDp(32),
    color: '#666666'
  },
  versionNum: {
    fontSize: pxToDp(28),
    color: '#666666'
  },
  line: {
    height: pxToDp(1),
    width: '100%',
    borderBottomWidth: pxToDp(1),
    borderBottomColor: '#eee',
    marginTop: pxToDp(120)
  },
  name: {
    fontSize: pxToDp(32),
    color: '#666',
    marginLeft: pxToDp(120),
    marginTop: pxToDp(24)
  }
})
const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Aside)