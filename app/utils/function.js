import Toast from 'react-native-toast'

function Name (name) {
  let length = name.length
  if ( length ==0 ) {
    Toast.showShortBottom( '昵称不能为空')
    return false
  }
  if ( length > 16 ) {
    Toast.showShortBottom( '昵称不能超过16位')
    return false
  }
  let reg = /^[\u4e00-\u9fa5A-Za-z0-9-_]*$/
  if (!reg.test(name)) {
    Toast.showShortBottom( '昵称中包含非法字符')
    return false
  }
  return true
}
function Phone (e) {
  if (isNaN(e)) {
    Toast.showShortBottom( '手机号码必须为数字')
    return false
  }

  if (!e || e.length == 0) {
    Toast.showShortBottom( '请输入手机号')
    return false
  }
  if (e.length !== 11) {
    Toast.showShortBottom( '手机号码长度错误')
    return false
  }
  return true
}

function Code (e) {
  if (isNaN(e)) {
    Toast.showShortBottom( '验证码必须为数字')
    return false
  }
  if (e.length == 0) {
    Toast.showShortBottom( '请输入验证码')
    return false
  }
  if (e.length !== 4) {
    Toast.showShortBottom( '验证码长度错误')
    return false
  }
  return true
}

function Password (e,eNew = '') {
  let length = e.length
  if (length < 6) {
    Toast.showShortBottom( '密码至少为6位')
    return false
  }
  if (length > 18) {
    Toast.showShortBottom( '密码不能超过18位')
    return false
  }
  let reg = /^[a-zA-Z1-9]\w{5,17}$/
  if (!reg.test(e)) {
    Toast.showShortBottom( '密码中不能包含中文和非法字符')
    return false
  }
  if(eNew && eNew != e) {
    Toast.showShortBottom( '重复密码与新密码不一致')
    return false
  }
  return true
}
function mail (e,v = "") {
  if(e === v || !e){
    Toast.showShortBottom( '请输入新邮箱')
    return false
  }
  let reg = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/
  if (!reg.test(e)) {
    Toast.showShortBottom( '邮箱格式不正确')
    return false
  }
  return true
}
const hidePhone = (phone) => {
  if (!phone) return '***********'
  if (phone.length != 11) {
    return phone
  } else {
    let reg=/([0-9]{3})([0-9]{4})([0-9]{4})/g;
    let newPhone = phone.replace(reg,"$1"+"****"+"$2")
    return newPhone
  }
}

const timeOut = (ms) => {
  return new Promise((resolve, reject) => {
    setTimeout(reject, ms, 'wait timeout')
  })
}

const check = {
  Name,
  Phone,
  Password,
  mail,
  Code
}

const tool = {
  hidePhone,
  timeOut
}

export {check,tool}

