import store from 'react-native-simple-store'
import NavigatorService from '../navigatorService'
import { NetInfo } from 'react-native'
import CryptoJS from 'crypto-js/crypto-js'

let isConnected = true

NetInfo.isConnected.fetch().then(connected => {
  isConnected = connected
})

function handleConnectivityChange (connected) {
  isConnected = connected
}

NetInfo.isConnected.addEventListener(
  'connectionChange',
  handleConnectivityChange
)

// AES解密函数
// var key = 'ancght3597648950'// 16位
var key = 'abcdef0123456789'// 16位
key = CryptoJS.enc.Utf8.parse(key)

let aesDecrypt = function (encrypted, key) {
  let decrypted = CryptoJS.AES.decrypt(encrypted, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })
  decrypted = CryptoJS.enc.Utf8.stringify(decrypted)// 转换为 utf8 字符串
  return decrypted
}

// AES加密函数
let aesEncrypt = function (data, key) {
  var encrypted = CryptoJS.AES.encrypt(data, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })
  return encrypted.toString()
}

async function setHeader (opts) {
  return store.get('user').then((user) => {
    if (user && user.token) {
      if (!opts.headers) opts.headers = {}
      // opts.headers['uid'] = user.uid
      opts.headers['Authorization'] = user.token
    }
    return opts
  }).catch(() => {
    return opts
  })
}

function _fetch(fetch, timeout) {
  return Promise.race([
    fetch,
    new Promise(function (resolve, reject) {
      setTimeout(() => reject(new Error('request timeout')), timeout);
    })
  ])
}
// 正常调用
async function request (url, method, body, slience) {
  console.log(url,method,body,slience)
  let opts = {
    method: method,
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    rejectUnauthorized: false
  }
  opts = await setHeader(opts)
  if (method.toUpperCase() === 'GET') {
    url = setParams(url, body)
  } else {
    if (method.toUpperCase() === 'POST') {
      opts.body = JSON.stringify(body)
    }
    opts.body = aesEncrypt(opts.body, key)
    // opts.body = body
  }
  const TIMEOUT = 50000
  // console.log(url)
  return new Promise((resolve, reject) => {
    _fetch(fetch(url, opts),TIMEOUT).then((response) => {
      console.log(response)
      if (response.ok) {
        response.json().then((res) => {
          let data
          if (typeof (res) === 'object') {
            data = res
          } else {
            data = JSON.parse(aesDecrypt(res, key))
          }
          if (data.meta.code === 200) {
            resolve(data.data)
          } else {
            reject(new Error(data.meta.message))
          }
        })
      } else {
        if (!slience && response.status === 401) {
          NavigatorService.navigate('Auth')
          response.json().then((res) => {
            res.meta.code = 401
            reject(new Error(res.meta.message))
          })
        } else {
          response.json().then((res) => reject(new Error(res.meta.message))).catch(() => reject(new Error('服务器异常')))
        }
      }
    }).catch((error) => {
      // console.log(error,url,opts)
      if (isConnected) {
        error = '服务器异常'
      } else {
        error = '网络异常'
      }
      reject(new Error(error))
    })
  })
}


// GET请求添加参数到url内
let setParams = function (url, obj) {
  if (!obj) return url
  let str = []
  for (let p in obj) {
    if (obj.hasOwnProperty(p) && obj[p] !== undefined) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
    }
  }
  if (url.indexOf('?') !== -1) {
    return url + '&' + str.join('&')
  } else {
    return url + '?' + str.join('&')
  }
}
const http = {
  request: request,
  setParams
}
export default http
