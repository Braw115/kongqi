import React from 'react'
import {
  Dimensions,
  PixelRatio,
  // Platform,
  // StatusBar,
  View
} from 'react-native'

let props = {}
export default class Resolution {
  static get (useFixWidth = true) {
    return useFixWidth ? {...props.fw} : {...props.fh}
  }

  static setDesignSize (dwidth = 750, dheight = 1336, dim = 'window') {
    let designSize = {width: dwidth, height: dheight}
    // let navHeight = Platform.OS === 'android' ? StatusBar.currentHeight : 0
    let pxRatio = PixelRatio.get()
    let {width, height} = Dimensions.get(dim)
    // if (dim !== 'screen') height -= navHeight
    let w = PixelRatio.getPixelSizeForLayoutSize(width)
    let h = PixelRatio.getPixelSizeForLayoutSize(height)
    let fwDesignScale = designSize.width / w
    let fwWidth = designSize.width
    let fwHeight = h * fwDesignScale
    let fwScale = 1 / pxRatio / fwDesignScale

    let fhDesignScale = designSize.height / h
    let fhWidth = w * fhDesignScale
    let fhHeight = designSize.height
    let fhScale = 1 / pxRatio / fhDesignScale

    props.fw = {width: fwWidth, height: fwHeight, scale: fwScale}
    props.fh = {width: fhWidth, height: fhHeight, scale: fhScale}
  }

  static keepSize = (size) => {
    return size / Resolution.get().scale
  }

  static FixWidthView = (p) => {
    let {width, height, scale} = props.fw
    return (
      <View {...p} style={[p.style, {
        // marginTop: navHeight,
        width: width,
        height: height,
        transform: [{translateX: -width * 0.5},
          {translateY: -height * 0.5},
          {scale: scale},
          {translateX: width * 0.5},
          {translateY: height * 0.5}]
      }]} />
    )
  }

  static FixHeightView = (p) => {
    let {width, height, scale, navHeight} = props.fh
    return (
      <View {...p} style={[p.style, {
        marginTop: navHeight,
        width: width,
        height: height,
        transform: [{translateX: -width * 0.5},
          {translateY: -height * 0.5},
          {scale: scale},
          {translateX: width * 0.5},
          {translateY: height * 0.5}]
      }]} />
    )
  }
};
// init
Resolution.setDesignSize()
