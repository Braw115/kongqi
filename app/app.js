import React, { Component } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native'
import NavigatorService from './navigatorService'
import Device from './pages/Device'
import My from './pages/My'
import PasswordChange from './pages/PasswordChange'
import PhoneChange from './pages/PhoneChange'
import PasswordInfoChange from './pages/PasswordInfoChange'
import MailBox from './pages/MailBox'
import Register from './pages/Register'
import Login from './pages/Login'
import Message from './pages/Message'
import MessageDetails from './pages/MessageDetails'
import AboutUs from './pages/AboutUs'
import PersonInfo from './pages/PersonInfo'
import Splash from './pages/Splash'
import Sensor from './pages/Sensor'
import SetDevice from './pages/SetDevice'
import Pair from './pages/Pair'
import { createStackNavigator, createBottomTabNavigator, SafeAreaView ,createAppContainer} from 'react-navigation'
import pxToDp from './utils/pxToDp'
import TabIcon from './components/TabIcon'

const TabNavigator = createBottomTabNavigator({
    Device: {
      screen: Device,
      navigationOptions: {
        tabBarOptions:{showLabel:false},
        tabBarIcon: ({tintColor, focused}) => (
          <TabIcon focused={focused}
             title='设备'
             active={require('./images/device/deviceFocus.png')}
             inActive={require('./images/device/deviceBlur.png')}
          />
        )
      }
    },
    My: {
      screen: My,
      navigationOptions: {
        tabBarOptions:{showLabel:false},
        tabBarIcon: ({tintColor, focused}) => (
          <TabIcon focused={focused}
             title='我的'
             active={require('./images/device/myFocus.png')}
             inActive={require('./images/device/myBlur.png')}
          />
        ),
      }
    }
  },{
    initialRouteName:'Device'
  }
)

const AppNavigator = createStackNavigator({
  TabNavigator,
  Device,
  Login,
  PasswordChange,
  AboutUs,
  PhoneChange,
  PasswordInfoChange,
  MailBox,
  Register,
  Message,
  MessageDetails,
  PersonInfo,
  Splash,
  Sensor,
  Pair,
  SetDevice
},{
  initialRouteName: 'Splash',
  mode: 'card',
  headerMode: 'none'
})

const AppContainer = createAppContainer(AppNavigator)
class App extends Component {
  render () {
    return (
      <SafeAreaView style={styles.container}>
        <AppContainer ref={navigatorRef => {
          NavigatorService.setNavigator(navigatorRef)
        }} />
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
export default App
