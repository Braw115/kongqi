import React from 'react'
import { ScrollView, FlatList, View, StyleSheet, Text, TouchableOpacity, Image, StatusBar } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'
import moment from 'moment'

class HistoryRecord extends React.Component {
  constructor (props) {
    super(props)
  }
  componentDidMount () {
    const { navigation, actions } = this.props
    this._didFocusSubscription =  navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content', true)
      StatusBar.setBackgroundColor('transparent')
      const { myInfo, defaultDevice } = this.props.publicReducer
      if(defaultDevice!=''){
        let body = {
          uid: myInfo.uid,
          mac: defaultDevice,
          startTime: moment().subtract('month').format('YYYY-MM-DD hh:mm:ss'),
          endTime: moment().format('YYYY-MM-DD hh:mm:ss'),
          currentPage: 1,
          currentSize: 100
        }
        actions.getHistory(body)
      }
    })
  }

  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }

  render () {
    const { navigation, publicReducer } = this.props
    const { history, defaultDevice } = publicReducer
    console.log({history})
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <Header
          back={() => navigation.navigate('Home')}
          title={'历史记录'}
        />
      {history.length ?
        <View>
        <View style={styles.topBox}>
          <Text style={styles.mac}>设备MAC地址</Text>
          <TouchableOpacity style={styles.macBox} onPress={ () => navigation.navigate('ChooseDevice')} >
            <Text style={styles.macText}>{defaultDevice}</Text>
            <Image style={styles.dropImage} source={require('../images/drop.png')} />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <View style={styles.title}>
            <Text style={[styles.date, styles.titleFont]}>日期</Text>
            <Text style={[styles.aqi, styles.titleFont]}>PM1.0</Text>
            <Text style={[styles.pm25, styles.titleFont]}>PM2.5</Text>
            <Text style={[styles.pm10, styles.titleFont]}>PM10</Text>
          </View>
          <View style={styles.container} >
            { history.map((item, index) => {
              return (
                <View style={styles.dataLine} key={index}>
                  <Text style={[styles.date, styles.dateFont]}>{moment(item.createTime||Date.now()).format('MM-DD')}</Text>
                  <Text style={[styles.aqi, styles.contentFont]}>{item.pm1_0}<Text style={styles.unit}>ug/m3</Text></Text>
                  <Text style={[styles.pm25, styles.contentFont]}>{item.pm2_5}<Text style={styles.unit}>ug/m3</Text></Text>
                  <Text style={[styles.pm10, styles.contentFont]}>{item.pm10}<Text style={styles.unit}>ug/m3</Text></Text>
                </View>
              )
            })}
          </View>
        </View>
        </View>:
          <View style={styles.noDataBox}>
            <Image style={styles.noData} source={require('../images/data.png')} />
            <Text>暂无数据</Text>
          </View>
        }
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  title: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: pxToDp(30)
  },
  dataLine: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: pxToDp(30)
  },
  titleFont: {
    fontSize: pxToDp(32),
    color: '#999',
    textAlign: 'left'
  },
  unit: {
    fontSize: pxToDp(24),
    color: '#666'
  },
  dateFont: {
    fontSize: pxToDp(32),
    color: '#666',
    textAlign: 'left'
  },
  contentFont: {
    fontSize: pxToDp(36),
    color: '#666',
    textAlign: 'left',
    fontWeight: '400'
  },
  date: {
    width: pxToDp(120),
    marginLeft: pxToDp(44)
  },
  aqi: {
    width: pxToDp(150),
    marginLeft: pxToDp(40)
  },
  pm25: {
    width: pxToDp(150),
    marginLeft: pxToDp(40)
  },
  pm10: {
    width: pxToDp(150),
    marginLeft: pxToDp(40)
  },
  topBox: {
    height: pxToDp(100),
    borderColor: '#fff',
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: pxToDp(1)
  },
  macBox: {
    width: pxToDp(300),
    height: pxToDp(40),
    backgroundColor: '#fff',
    position: 'absolute',
    right: pxToDp(0),
    flexDirection: 'row',
    alignItems: 'center'
  },
  dropBox: {
    height: pxToDp(40),
    marginRight: pxToDp(14)
  },
  dropImage: {
    width: pxToDp(25),
    height: pxToDp(14)
  },
  macText: {
    color: '#999',
    fontSize: pxToDp(28),
    textAlign: 'right',
    marginRight:pxToDp(18)
  },
  macLine: {
    width: pxToDp(190),
    borderColor: '#eee',
    borderWidth: 2,
    borderRadius: 3
  },
  dropText: {
    fontSize: pxToDp(24)
  },
  noData: {
    width: pxToDp(120),
    height: pxToDp(120)
  },
  noDataBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: pxToDp(300)
  },
  mac: {
    color: '#666',
    fontSize: pxToDp(32),
    marginLeft: pxToDp(40)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryRecord)