import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, TouchableOpacity, Text } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import Toast from 'react-native-toast'
import Loading from '../components/Loading'
import {check} from '../utils/function'
import store from 'react-native-simple-store'

const Verify = (props) => (
  <TouchableOpacity disabled={!props.verify} style={props.verify ? styles.codeOn : styles.codeOff} onPress={props.onPress} >
    <Text style={props.verify ? styles.codeTextOn : styles.codeTextOff}>{props.verifyText}</Text>
  </TouchableOpacity>
)

class PasswordChange extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      phone: '',
      password: '',
      code: '',
      codeTime: 0,
      nowTime: 0
    }
  }

  componentDidMount () {
    this._didFocusSubscription =  this.props.navigation.addListener('didFocus', () => {
      //设置倒计时
      store.get('codeTime').then((codeTime) => {
        this.setState({
          codeTime: codeTime,
          nowTime: new Date().getTime()
        })
        this.interval && clearInterval(this.interval)
        const _this = this
        this.interval = setInterval(() => {
          let nowTime = new Date().getTime()
          if (nowTime > codeTime) {
            _this.interval && clearInterval(_this.interval)
          } else {
            this.setState({nowTime: nowTime})
          }
        }, 1000)
      }).catch()
    })
  }
  componentWillUnmount () {
    this.interval && clearInterval(this.interval)
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }
  onSubmit = () => {
    const { actions, navigation } = this.props
    const { phone, password, code } = this.state
    if(check.Phone(phone) && check.Password(password) && check.Code(code)){
      let body = { phone,password,code }
      actions.forgetPassword(body)
    }
  }
  getCode = () => {
    const phone = this.state.phone
    const {actions} = this.props
    if (check.Phone(phone)) {
      let body = {phone}
      actions.sendCode(body).then((res) => {
        console.log(res)
        let finishTime = new Date().getTime() + 60000
        store.save('codeTime', {finishTime})
        this.setState({
          codeTime: finishTime
        })
        this.interval && clearInterval(this.interval)
        this.interval = setInterval(() => {
          let nowTime = new Date().getTime()
          if (nowTime > finishTime+500) {
            this.interval && clearInterval(this.interval)
          } else {
            this.setState({nowTime: nowTime})
          }
        }, 1000)
      }).catch()
    }
  }
  render () {
    const { navigation, publicReducer } = this.props
    const { phone, password, code, codeTime, nowTime } = this.state
    const verify = codeTime > nowTime ? false: true // false 表示不能发送验证码
    const verifyText = verify ? '获取验证码' : Math.ceil((codeTime - new Date().getTime()) / 1000) + 's'
    return (
      <View style={styles.container}>
        <Header back={() => navigation.goBack()} title={'忘记密码'} />
        <ScrollView style={styles.container} >
          <TextInput
            style={[styles.TextInputBox, { marginTop: pxToDp(280) }]}
            placeholder='手机号'
            autoFocus
            value={phone}
            underlineColorAndroid='transparent'
            placeholderTextColor='#999999'
            onChangeText={(text) => this.setState({ phone: text })}
            onEndEditing={() => { check.Phone(phone) }}
          />
          <TextInput
            style={styles.TextInputBox}
            placeholder='请输入新密码'
            underlineColorAndroid='transparent'
            secureTextEntry
            value={password}
            maxLength={11}
            placeholderTextColor='#999999'
            onChangeText={(text) => this.setState({ password: text })}
            onEndEditing={() => { check.Password(password) }}
          />

          <View style={styles.TextInputThree}>
            <TextInput
              style={[styles.TextInputBox, { flex: 1, marginHorizontal: 0 }]}
              placeholder='请输入验证码'
              underlineColorAndroid='transparent'
              maxLength={4}
              onChangeText={(text) => this.setState({ code: text })}
              placeholderTextColor='#999999'
              vlaue={code}
            />
            <Verify onPress={this.getCode} verifyText={verifyText} verify={verify} />
          </View>

          <Button
            title='确定'
            buttonStyle={styles.buttonStyle}
            onPress={this.onSubmit}
          />
        </ScrollView>
        <Loading show={publicReducer.loading} text={'提交中'} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  TextInputBox: {
    height: pxToDp(80),
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#d8d8d8',
    alignItems: 'center',
    paddingVertical: 0,
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(30),
    fontSize: pxToDp(32)
  },
  TextInputThree: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(150)
  },
  code: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#dddddd'
  },
  codeText: {
    color: '#999999',
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    marginHorizontal: pxToDp(30)
  },
  codeOff: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#dddddd'
  },
  codeOn: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#3FABFC'
  },
  codeTextOff: {
    color: '#999',
    fontSize: pxToDp(32)
  },
  codeTextOn: {
    color: '#fff',
    fontSize: pxToDp(32)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordChange)
