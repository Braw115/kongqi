import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, FlatList } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'

class historyRecord extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      AQI: true,
      PM2_5: false,
      PM10: false
    }
  }

  selectLeft = () => {
    this.setState({
      AQI: true,
      PM2_5: false,
      PM10: false
    })
  }

  selectMiddle = () => {
    this.setState({
      PM2_5: true,
      AQI: false,
      PM10: false
    })
  }

  selectRight = () => {
    this.setState({
      PM10: true,
      AQI: false,
      PM2_5: false
    })
  }

  _renderHead = () => {
    let { AQI, PM2_5, PM10 } = this.state

    return (
      <View style={styles.title}>
        <TouchableOpacity
          style={styles.titleWidth}
          onPress={() => { this.selectLeft() }}
        >
          {
            AQI === true ? <Text style={styles.titleTextBig}>AQI</Text> : <Text style={styles.titleText}>AQI</Text>
          }
          <Text style={styles.titleText}>AQI</Text>
          {
            AQI === true ? <View style={styles.titleBottomLine} /> : null
          }
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.titleWidth}
          onPress={() => { this.selectMiddle() }}
        >
          <Text style={styles.titleText}>PM2.5</Text>
          {
            PM2_5 === true ? <View style={styles.titleBottomLine} /> : null
          }
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.titleWidth}
          onPress={() => { this.selectRight() }}
        >
          <Text style={styles.titleText}>PM10</Text>
          {
            PM10 === true ? <View style={styles.titleBottomLine} /> : null
          }
        </TouchableOpacity>
      </View>

    )
  }

  _renderItem = () => {
    return (
      <View style={styles.details}>
        <Text style={styles.deLeft}>07-05</Text>
        <Text style={styles.deMiddle}>星期五</Text>
        <Text style={styles.deRightAQI}>90</Text>
      </View>
    )
  }

  _renderEmpty = () => {
    return (
      <View style={{ backgroundColor: 'red', flex: 1 }}>
        <Text >07-05</Text>
      </View>
    )
  }

  render () {
    const { navigation } = this.props

    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.navigate('Home')}
          title={'历史记录'}
        />

        {/* <FlatList
          data={[{ key: 'a' }, { key: 'b' }]}
          renderItem={this._renderItem}
          ListHeaderComponent={this._renderHead}
        /> */}

        <FlatList
          contentContainerStyle={styles.listContainer}
          renderItem={this._renderItem}
          ListHeaderComponent={this._renderEmpty}
        />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  title: {
    flexDirection: 'row',
    marginTop: pxToDp(30)
  },
  titleWidth: {
    flex: 1,
    alignItems: 'center'
  },
  titleText: {
    textAlign: 'center'
  },
  titleTextBig: {
    color: '#333',
    textAlign: 'center'
  },
  titleBottomLine: {
    width: pxToDp(50),
    height: pxToDp(8),
    backgroundColor: '#3FABFC',
    borderRadius: pxToDp(4),
    marginTop: pxToDp(20)
  },
  details: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(50),
    marginVertical: pxToDp(20)
  },
  deLeft: {
    width: pxToDp(86)
  },
  deMiddle: {
    flex: 1
  },
  deRight: {
    width: pxToDp(120)
  },
  listContainer: {
    flex: 1,
    paddingBottom: 20
  }
})

export default historyRecord
