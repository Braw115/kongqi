import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, TouchableOpacity, Text } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import store from 'react-native-simple-store'
import Loading from '../components/Loading'
import { check } from '../utils/function'

class MailBox extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      mail: '',
      uid: ''
    }
  }

  componentDidMount () {
    console.log(this.props.navigation)
    store.get('user').then(res => {
      this.setState({ uid: res.uid })
    })
  }

  onSubmit = () => {
    const { actions, navigation } = this.props
    const { mail, uid } = this.state
    const email = navigation.getParam('mail')
    let body = { mail,uid }
    if(check.mail(mail,email)){
      actions.modifyPersonInfo(body).then(res => {
        console.log(res)
        navigation.goBack()
      })
    }
  }

  render () {
    const { navigation, publicReducer } = this.props
    const email = navigation.getParam('mail')
    const { mail } = this.state
    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.goBack()}
          title={mail?'修改邮箱':'绑定邮箱'}
        />
        <ScrollView style={styles.container} >
          <TextInput
            style={[styles.TextInputBox, { marginTop: pxToDp(280),marginBottom:pxToDp(200) }]}
            placeholder='请输入邮箱'
            selectTextOnFocus
            autoFocus
            keyboardType={'email-address'}
            underlineColorAndroid='transparent'
            placeholderTextColor='#999999'
            onChangeText={(text) => this.setState({ mail: text })}
          />
          {/*<View style={styles.TextInputThree}>*/}
            {/*<TextInput*/}
              {/*style={[styles.TextInputBox, { flex: 1, marginHorizontal: 0 }]}*/}
              {/*placeholder='请输入验证码'*/}
              {/*underlineColorAndroid='transparent'*/}
              {/*placeholderTextColor='#999999'*/}
            {/*/>*/}
            {/*<TouchableOpacity style={styles.code}>*/}
              {/*<Text style={styles.codeText}>获取验证码</Text>*/}
            {/*</TouchableOpacity>*/}
          {/*</View>*/}
          <Button
            title='确定'
            buttonStyle={styles.buttonStyle}
            onPress={this.onSubmit}
          />
        </ScrollView>
        <Loading show={publicReducer.loading} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  TextInputBox: {
    height: pxToDp(80),
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#d8d8d8',
    alignItems: 'center',
    paddingVertical: 0,
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(30),
    fontSize: pxToDp(32)
  },
  TextInputThree: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(150)
  },
  code: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#dddddd'
  },
  codeText: {
    color: '#999999',
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    marginHorizontal: pxToDp(30)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MailBox)
