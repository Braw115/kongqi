import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, TouchableOpacity, Text } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import Toast from 'react-native-toast'
import Loading from '../components/Loading'
import store from 'react-native-simple-store'
import { check } from '../utils/function'
import { NavigationActions } from 'react-navigation'

// import store from 'react-native-simple-store'
const Verify = (props) => (
  <TouchableOpacity disabled={!props.verify} style={props.verify ? styles.codeOn : styles.codeOff} onPress={props.onPress} >
    <Text style={props.verify ? styles.codeTextOn : styles.codeTextOff}>{props.verifyText}</Text>
  </TouchableOpacity>
)
class PasswordInfoChange extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      oldPsw: '',
      newPsw: '',
      againPsw: '',
      uid: '',
      code: '',
      codeTime: 0,
      nowTime: 0,
      phone: ''
    }
  }
  componentDidMount () {
    store.get('user').then(res => {
      const { uid='',phone='' } = res
      this.setState({ uid, phone })
    }).catch()
    this._didFocusSubscription =  this.props.navigation.addListener('didFocus', () => {
      //设置倒计时
      store.get('codeTime').then((codeTime) => {
        this.setState({
          codeTime: codeTime,
          nowTime: new Date().getTime()
        })
        if(codeTime > Date.now()){
          this.interval && clearInterval(this.interval)
          this.interval = setInterval(() => {
            let nowTime = new Date().getTime()
            if (nowTime > codeTime) {
              this.interval && clearInterval(this.interval)
            } else {
              this.setState({nowTime: nowTime})
            }
          }, 1000)
        }
      }).catch()
    })
  }
  getCode = () => {
    const phone = this.state.phone
    const {actions} = this.props
    if (check.Phone(phone)) {
      let body = {phone}
      actions.sendCode(body).then((res) => {
        console.log(res)
        let finishTime = new Date().getTime() + 60000
        store.save('codeTime', {finishTime})
        this.setState({
          codeTime: finishTime
        })
        this.interval && clearInterval(this.interval)
        this.interval = setInterval(() => {
          let nowTime = new Date().getTime()
          if (nowTime > finishTime+900) {
            this.interval && clearInterval(this.interval)
          } else {
            this.setState({nowTime: nowTime})
          }
        }, 1000)
      }).catch()
    }
  }
  componentWillUnmount () {
    this.interval && clearInterval(this.interval)
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }
  onSubmit = () => {
    const { actions, navigation } = this.props
    const { oldPsw, newPsw, againPsw, code, phone } = this.state
    if(check.Password(oldPsw) && check.Password(newPsw,againPsw) && check.Code(code)){
      if(!againPsw){
        Toast.showShortBottom('重复密码不能为空')
        return
      }
      if(oldPsw === newPsw){
        Toast.showShortBottom('新密码不能和原密码相同')
        return
      }
      let body = {phone,code,password:oldPsw,confirmPassword:newPsw}
      actions.modifyPassword(body).then(() => {
        Toast.showShortBottom('修改成功，请重新登录')
        navigation.reset([
          NavigationActions.navigate({routeName: 'Splash',params:{unInit:true}}),
          NavigationActions.navigate({routeName: 'Login'})
        ],1)
      })
    }
  }

  render () {
    const { navigation, publicReducer } = this.props
    const { oldPsw, newPsw, againPsw, code, codeTime, nowTime } = this.state
    const verify = codeTime > nowTime ? false: true // false 表示不能发送验证码
    const verifyText = verify ? '获取验证码' : Math.ceil((codeTime - new Date().getTime()) / 1000) + 's'
    console.log(verifyText)
    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.goBack()}
          title={'修改密码'}
        />
        <ScrollView style={styles.container} >
          <TextInput
            style={[styles.TextInputBox, { marginTop: pxToDp(280) }]}
            placeholder='请输入密码'
            secureTextEntry
            underlineColorAndroid='transparent'
            placeholderTextColor='#999999'
            onChangeText={(text) => this.setState({ oldPsw: text })}
            value={oldPsw}
          />
          <TextInput
            style={styles.TextInputBox}
            placeholder='请输入新密码'
            underlineColorAndroid='transparent'
            placeholderTextColor='#999999'
            secureTextEntry
            value={newPsw}
            onChangeText={(text) => this.setState({ newPsw: text })}
          />

          <TextInput
            style={[styles.TextInputBox, { marginBottom: pxToDp(30) }]}
            placeholder='请确认密码'
            underlineColorAndroid='transparent'
            placeholderTextColor='#999999'
            secureTextEntry
            value={againPsw}
            onChangeText={(text) => this.setState({ againPsw: text })}
          />
          <View style={styles.TextInputThree}>
            <TextInput
              style={[styles.TextInputBox, { flex: 1, marginHorizontal: 0 }]}
              placeholder='请输入验证码'
              underlineColorAndroid='transparent'
              maxLength={4}
              onChangeText={(text) => this.setState({ code: text })}
              placeholderTextColor='#999999'
              vlaue={code}
            />
            <Verify onPress={this.getCode} verifyText={verifyText} verify={verify} />
          </View>
          <Button
            title='确定'
            buttonStyle={styles.buttonStyle}
            onPress={this.onSubmit}
          />
        </ScrollView>
        <Loading show={publicReducer.loading} text={'提交中'} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  TextInputBox: {
    height: pxToDp(80),
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#d8d8d8',
    alignItems: 'center',
    paddingVertical: 0,
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(30),
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    marginHorizontal: pxToDp(30)
  },
  codeOff: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#dddddd'
  },
  codeOn: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#3FABFC'
  },
  codeTextOff: {
    color: '#999',
    fontSize: pxToDp(32)
  },
  codeTextOn: {
    color: '#fff',
    fontSize: pxToDp(32)
  },
  TextInputThree: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(150)
  },
  code: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#dddddd'
  },
  codeText: {
    color: '#999999',
    fontSize: pxToDp(32)
  },
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordInfoChange)
