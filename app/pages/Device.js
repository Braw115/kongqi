import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, StatusBar, ListView, TouchableWithoutFeedback } from 'react-native'
import pxToDp from '../utils/pxToDp'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'
import { SwipeListView } from 'react-native-swipe-list-view'
import Toast from 'react-native-toast'

class Device extends React.Component {
  constructor (props) {
    super (props)
  }

  componentDidMount () {
    const {navigation,actions,publicReducer} = this.props
    // actions.getCityData(publicReducer.city)
    this._willFocusSubscription =  navigation.addListener('willFocus', () => {
      StatusBar.setBarStyle('light-content', true)
    })
    this._didFocusSubscription =  navigation.addListener('didFocus', () => {
      actions.queryDeviceList(publicReducer.myInfo)
    })
    // this._willBlueSubscription =  navigation.addListener('willBlur', () => {
    //   StatusBar.setBarStyle('dark-content', true)
    // })
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
    this._willFocusSubscription && this._willFocusSubscription.remove()
    this._willBlueSubscription && this._willBlueSubscription.remove()
  }

  switchIcon = (data) => {
    const {type,status,wifi,mac,work,powerModel} = data
    //type 1 醛扇 2 醛三
    //status 0 离线 1 在线
    //powerModel 1 内部供电 2 外部供电
    //work 1 开机 2 待机 3 关机
    console.log(data)
    if( status === 0 ){
      if(powerModel==1 && work==2){
        return require('../images/device/sleep.png')
      }
      return require('../images/device/offLine.png')
    }
    if( powerModel == 2 && work === 2 ){
      return require('../images/device/sleep.png')
    }
    switch (wifi) {
      case 0:
        return require('../images/device/wifi1.png')
        break
      case 1:
        return require('../images/device/wifi1.png')
        break
      case 2:
        return require('../images/device/wifi2.png')
        break
      case 3:
        return require('../images/device/wifi3.png')
        break
    }
  }

  unbind = (item) => {
    const { actions, publicReducer } = this.props
    let body = { uid: publicReducer.myInfo.uid, mac: item.mac}
    actions.unBindDevice(body)
  }
  buttonArea = {
    top:pxToDp(20),
    right:pxToDp(20),
    bottom:pxToDp(20),
    left:pxToDp(20)
  }
  render () {
    const { navigation, publicReducer } = this.props
    const { devices, cityData, city } = publicReducer
    const {aqi='---',pm2_5,pm10,quality} = cityData
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    const icons = [require('../images/device/quanshan.png'),require('../images/device/quansan.png')]
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={require('../images/device/background.png')} style={{position:'absolute',width:pxToDp(750),height:pxToDp(520)}} />
          <View style={styles.headerLine}>
            <TouchableOpacity style={{width:pxToDp(44),height:pxToDp(38),position:'absolute',left:pxToDp(39)}} onPress={()=>navigation.navigate('Message')} hitSlop={this.buttonArea} >
              <Image source={require('../images/device/message.png')} style={{width:pxToDp(44),height:pxToDp(38)}} />
            </TouchableOpacity>
            <Text style={styles.title}>空气质量指数(AQI)</Text>
            <TouchableOpacity style={{width:pxToDp(36),height:pxToDp(36),position:'absolute',right:pxToDp(23)}}  onPress={()=>navigation.navigate('Pair')} hitSlop={this.buttonArea} >
              <Image source={require('../images/device/add.png')} style={{width:pxToDp(36),height:pxToDp(36)}} />
            </TouchableOpacity>
          </View>
          <View style={{width:pxToDp(272),height:pxToDp(272),justifyContent:'center',alignItems:'center',marginTop:pxToDp(-40)}}>
            <Image source={require('../images/device/circle.png')} style={{width:pxToDp(272),height:pxToDp(272),position:'absolute'}} />
            <Text style={{fontSize:pxToDp(80),fontWeight:'400',color:'#fff'}}>{aqi}</Text>
            <Text style={{fontSize:pxToDp(36-quality.length*2),fontWeight:'500',color:'#fff'}}>{quality}</Text>
          </View>
          <View style={{width:pxToDp(750),flexDirection:'row',justifyContent: 'space-between',alignItems:'center',bottom:pxToDp(35)}}>
            <Text style={[styles.text,{left:pxToDp(36)}]}>{city}</Text>
            <Text style={styles.text}>PM2.5 <Text style={styles.value}>{pm2_5}</Text>ug/m³</Text>
            <Text style={[styles.text,{right:pxToDp(29)}]}>PM10 <Text style={styles.value}>{pm10}</Text>ug/m³</Text>
          </View>
        </View>
        <View style={{flex:1,backgroundColor:'#FBFBFB'}} >
          <View>
            <SwipeListView
              closeOnRowPress={true}
              closeOnScroll={true}
              enableEmptySections
              dataSource={ds.cloneWithRows(devices)}
              renderRow={(data) => {
                const {type,name,wifi,mac,status,work} = data
                return <View style={{width:pxToDp(750),height:pxToDp(142)}}>
                  <TouchableWithoutFeedback onPress={()=>navigation.navigate('Sensor',{mac,status,type,name})}>
                    <View  style={styles.deviceBox} >
                      <Image source={icons[type-1]} style={{width:pxToDp(110),height:pxToDp(140),position:'absolute',left:pxToDp(26)}} />
                      <Text style={{position:'absolute',left:pxToDp(159),fontSize:pxToDp(32),color:'#333'}} >{name||mac}</Text>
                      <Image source={this.switchIcon(data)} style={{width:pxToDp(70),height:pxToDp(70),position:'absolute',right:pxToDp(24)}} />
                    </View>
                  </TouchableWithoutFeedback>
                  <View style={styles.line}></View>
                </View>
              }}
              renderHiddenRow={(data, secdId, rowId, rowMap) => (
                <TouchableOpacity style={styles.deleteMenu} onPress={() => {rowMap[`${secdId}${rowId}`].closeRow();this.unbind(data)}}>
                  <Image source={require('../images/device/delete.png')} style={{width:pxToDp(28),height:pxToDp(37)}} />
                </TouchableOpacity>
              )}
              disableRightSwipe
              rightOpenValue={pxToDp(-120)}
            />
          </View>
          <Text style={{width:pxToDp(750),textAlign:'center',marginTop:pxToDp(20),fontSize:pxToDp(26),color:'#c1c1c1'}}>没有更多设备了</Text>
        </View>
      </View>
    )
  }
}

const marginTop = getStatusBarHeight(true)
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header:{
    width:pxToDp(750),
    height:pxToDp(520),
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  headerLine: {
    width:pxToDp(750),
    height:pxToDp(50),
    flexDirection:'row',
    justifyContent: 'center',
    alignItems:'center',
    marginTop
  },
  title:{
    color:'#fff',
    fontSize:pxToDp(24),
    fontWeight:'400'
  },
  text:{
    color:'#fff',
    fontSize:pxToDp(24),
  },
  value:{
    color:'#fff',
    fontSize:pxToDp(36),
    paddingLeft: pxToDp(13)
  },
  deviceBox: {
    width:pxToDp(750),
    height:pxToDp(140),
    alignItems:'center',
    flexDirection:'row',
    backgroundColor:'#fff',
    zIndex: 30
  },
  line: {
    height:1,
    width:pxToDp(750),
    backgroundColor:'#D8D8D8',
    opacity:0.5783
  },
  deleteMenu:{
    backgroundColor:'#FF6B6B',
    width:pxToDp(120),
    height:pxToDp(140),
    position:'absolute',
    right:0,
    justifyContent:'center',
    alignItems:'center',
    zIndex: 0
  }
})
const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Device)
