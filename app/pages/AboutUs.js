import React from 'react'
import { ScrollView, View, StyleSheet, Text, Image, StatusBar } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'

class AboutUs extends React.Component {
  constructor (props) {
    super(props)
  }
  componentDidMount () {
    const { navigation, actions } = this.props
    this._didFocusSubscription =  navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content', true)
      StatusBar.setBackgroundColor('transparent')
    })
    actions.getAboutUs()
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }
  render () {
    const { navigation, publicReducer } = this.props
    const { aboutUs }= publicReducer
    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.goBack()}
          title={'关于我们'}
        />
        <View style={styles.top}>
          <Image style={styles.logo} source={require('../images/logo2.png')} />
          <Image style={styles.logoText} source={require('../images/logoTextAboutUs.png')} />
        </View>
        <ScrollView style={styles.container} >
          <Text style={styles.title}>公司简介</Text>
          <Text style={styles.content}>&emsp;&emsp;{aboutUs.weContent}</Text>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  top: {
    height: pxToDp(340),
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: pxToDp(173),
    height: pxToDp(107)
  },
  logoText: {
    width: pxToDp(113),
    height: pxToDp(39)
  },
  title: {
    textAlign: 'center',
    color: '#333',
    fontSize: pxToDp(32)
  },
  content: {
    fontSize: pxToDp(32),
    color: '#333',
    marginHorizontal: pxToDp(30)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs)