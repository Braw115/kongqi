import React from 'react'
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  StatusBar,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native'
import pxToDp from '../utils/pxToDp'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'
import Header from '../components/Header/Header'
import Slider from '../components/Slider'
import {Switch} from  'react-native-switch'
import Picker from 'react-native-picker'
import Toast from 'react-native-root-toast'
const {height,width} = Dimensions.get("window")
const level = () => {
  const k = height/width
  if(k>1.8)   return 3
  if(k>16/9)   return 2
  if(k>1.6)   return 1
}
const Mask = ({show=false}) => {
  return show?<View style={{flex:1,position:'absolute',left:0,top:0,backgroundColor:'rgba(40,43,49,1)',height:'100%',width:'100%',opacity:0.6017}} />
  :null
}
class SetDevice extends React.Component {
  warnLevel = ['轻度污染','中度污染','重度污染','关闭报警']
  models = ['睡眠','微风','清凉','强劲']
  timmings = ['30min','60min','120min','关闭定时']
  standbyTimes = ['2min','5min','10min','30min','无待机']
  detectionTimes = ['5min','10min','30min','60min','无检测']
  deviceType = 1 //1是醛扇,2是醛三
  constructor (props) {
    super (props)
    const { orderParams } = props.publicReducer
    const {model,timming,alarm_pm,alarm_formaldehyde,light,alarmVoice,standbyTime,detectionTime} = orderParams
    console.log(orderParams)
    this.state = {...orderParams,picker:false}
    this.deviceType = props.navigation.getParam('type') || 1
  }
  pickerBackgroundColor = [255,255,255,1]
  pickerFontColor = [31, 31, 31, 1]
  componentDidMount () {
    const {navigation,actions,publicReducer} = this.props
    this._willFocusSubscription =  navigation.addListener('willFocus', () => {
      StatusBar.setBarStyle('dark-content', true)
    })
    this._didFocusSubscription =  navigation.addListener('didFocus', () => {

    })
    this._willBlueSubscription =  navigation.addListener('willBlur', () => {
      StatusBar.setBarStyle('light-content', true)
      this.state.picker = false
      Picker.hide()
    })
  }

  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
    this._willFocusSubscription && this._willFocusSubscription.remove()
    this._willBlueSubscription && this._willBlueSubscription.remove()
  }

  buttonArea = {
    top:pxToDp(20),
    right:pxToDp(20),
    bottom:pxToDp(20),
    left:pxToDp(750)
  }

  _renderItem = ({name,value,alarmVoice,onPress},key) => {
    return (
      <View key={key}>
        <TouchableOpacity disabled={!!alarmVoice} style={styles.lineBox} onPress={onPress}  >
          <Text style={styles.leftText}>{name}</Text>
          {value?<Text style={styles.rightText} onPress={onPress} >{value}</Text>:null}
          {!alarmVoice?
            <View style={[styles.rightIcon,{position:'absolute',right:pxToDp(34)}]}  >
              <Image source={require('../images/set/right.png')} style={styles.rightIcon} />
            </View>
            : <View style={styles.switch}>
                <Switch
                  value={this.state.alarmVoice===1}
                  onValueChange={(value) => this.setAlarmVoice(value)}
                  disabled={false}
                  activeText={'On'}
                  inActiveText={'Off'}
                  circleSize={pxToDp(40)}
                  barHeight={pxToDp(40)}
                  circleBorderWidth={0}
                  backgroundActive={'rgb(109,187,82)'}
                  backgroundInactive={'rgb(220,223,230)'}
                  circleActiveColor={'#fff'}
                  circleInActiveColor={'#fff'}
                  renderInsideCircle={() => <View style={{backgroundColor:'#fff'}}></View>} // custom component to render inside the Switch circle (Text, Image, etc.)
                  changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                  innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                  outerCircleStyle={{borderColor:'#fff'}} // style for outer animated circle
                  renderActiveText={false}
                  renderInActiveText={false}
                  switchLeftPx={pxToDp(4)} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                  switchRightPx={pxToDp(4)} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                  switchWidthMultiplier={pxToDp(4)} // multipled by the `circleSize` prop to calculate total width of the Switch
                  switchBorderRadius={pxToDp(40)} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
                />
            </View>
          }
        </TouchableOpacity>
        <View style={styles.line} />
      </View>
    )
  }

  setPMLevel = () => {
    Picker.init({
      pickerData: this.warnLevel,
      selectedValue: [this.warnLevel[this.state.alarm_pm]],
      pickerTitleText:'选择级别',
      pickerConfirmBtnText:"确定",
      pickerBg:this.pickerBackgroundColor,
      pickerFontColor:this.pickerFontColor,
      pickerCancelBtnText:"取消",
      onPickerConfirm: data => {
        this.setState({alarm_pm:this.warnLevel.indexOf(data[0])},()=>this.setOrder())
        this.setState({picker:!this.state.picker})
      },
      onPickerCancel: ()=>{
        this.setState({picker:!this.state.picker})
      }
    })
    Picker.show()
    this.setState({picker:!this.state.picker})
  }
  setFormaldehydeLevel = () => {
    Picker.init({
      pickerData: this.warnLevel,
      selectedValue: [this.warnLevel[this.state.alarm_formaldehyde]],
      pickerTitleText:'选择级别',
      pickerConfirmBtnText:"确定",
      pickerBg:this.pickerBackgroundColor,
      pickerFontColor:this.pickerFontColor,
      pickerCancelBtnText:"取消",
      onPickerConfirm: data => {
        this.setState({alarm_formaldehyde:this.warnLevel.indexOf(data[0])},()=>this.setOrder())
        this.setState({picker:!this.state.picker})
      },
      onPickerCancel: ()=>{
        this.setState({picker:!this.state.picker})
      }
    })
    this.setState({picker:!this.state.picker})
    Picker.show()
  }
  setModel = () => {
    Picker.init({
      pickerData: this.models,
      selectedValue: [this.models[this.state.model]],
      pickerTitleText:'选择模式',
      pickerConfirmBtnText:"确定",
      pickerBg:this.pickerBackgroundColor,
      pickerFontColor:this.pickerFontColor,
      pickerCancelBtnText:"取消",
      onPickerConfirm: data => {
        this.setState({model:this.models.indexOf(data[0])},()=>this.setOrder())
        this.setState({picker:!this.state.picker})
      },
      onPickerCancel: ()=>{
        this.setState({picker:!this.state.picker})
      }
    })
    Picker.show()
    this.setState({picker:!this.state.picker})
  }
  setTimming = () => {
    Picker.init({
      pickerData: this.timmings,
      selectedValue: [this.timmings[this.state.timming]],
      pickerTitleText:'选择定时',
      pickerConfirmBtnText:"确定",
      pickerBg:this.pickerBackgroundColor,
      pickerFontColor:this.pickerFontColor,
      pickerCancelBtnText:"取消",
      onPickerConfirm: data => {
        this.setState({timming:this.timmings.indexOf(data[0])},()=>this.setOrder())
        this.setState({picker:!this.state.picker})
      },
      onPickerCancel: ()=>{
        this.setState({picker:!this.state.picker})
      }
    })
    Picker.show()
    this.setState({picker:!this.state.picker})
  }
  setStandbyTime = () => {
    Picker.init({
      pickerData: this.standbyTimes,
      selectedValue: [this.standbyTimes[this.state.standbyTime]],
      pickerTitleText:'选择时长',
      pickerConfirmBtnText:"确定",
      pickerBg:this.pickerBackgroundColor,
      pickerFontColor:this.pickerFontColor,
      pickerCancelBtnText:"取消",
      onPickerConfirm: data => {
        this.setState({standbyTime:this.standbyTimes.indexOf(data[0])+1},()=>this.setOrder())
        this.setState({picker:!this.state.picker})
      },
      onPickerCancel: ()=>{
        this.setState({picker:!this.state.picker})
      }
    })
    Picker.show()
    this.setState({picker:!this.state.picker})
  }
  setDetectionTime = () => {
    Picker.init({
      pickerData: this.detectionTimes,
      selectedValue: [this.detectionTimes[this.state.detectionTime]],
      pickerTitleText:'选择时长',
      pickerConfirmBtnText:"确定",
      pickerBg:this.pickerBackgroundColor,
      pickerFontColor:this.pickerFontColor,
      pickerCancelBtnText:"取消",
      onPickerConfirm: data => {
        this.setState({detectionTime:this.detectionTimes.indexOf(data[0])+1},()=>this.setOrder())
        this.setState({picker:!this.state.picker})
      },
      onPickerCancel: ()=>{
        this.setState({picker:!this.state.picker})
      }
    })
    Picker.show()
    this.setState({picker:!this.state.picker})
  }
  setLightLevel = (light) => {
    console.log({light})
    this.setState({light},()=>this.setOrder())
  }
  setAlarmVoice = (value) => {
    this.setState({alarmVoice:value?1:2},()=>this.setOrder())
  }
  setTime = () => {
    this.props.actions.setTime(this.props.navigation.getParam('mac'))
    this.happyToast('校时指令已发出')
  }
  setOrder = () => {
    const { navigation, actions } = this.props
    let mac = navigation.getParam('mac')
    let type = navigation.getParam('type')
    let body = {
      mac,
      type,
      ...this.state
    }
    actions.setOrderParam(body)
    // this.happyToast()
  }
  happyToast = (message='指令已发出') => {
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position: 0,
      shadow: true,
      shadowColor:'#F4F4F4',
      backgroundColor:'#F4F4F4',
      textColor:'#333',
      animation: true,
      hideOnPress: true,
      delay: 0,
    })
  }
  render () {
    const { navigation, publicReducer } = this.props
    const { orderParams, deviceStatus } = publicReducer
    const {model,timming,alarm_pm,alarm_formaldehyde,light,alarmVoice,standbyTime,detectionTime} = this.state
    console.log(this.state)
    let lines = [
      [
        {name:'甲醛报警级别',value:this.warnLevel[alarm_formaldehyde],onPress:this.setFormaldehydeLevel},
        {name:'PM报警级别',value:this.warnLevel[alarm_pm],onPress:this.setPMLevel},
        {name:'模式设置',value:this.models[model],onPress:this.setModel},
        {name:'定时',value:this.timmings[timming],onPress:this.setTimming},
        {name:'报警声音开关',alarmVoice},
        {name:'设备校时',value:'',onPress:this.setTime}
      ],[
        {name:'甲醛报警级别',value:this.warnLevel[alarm_formaldehyde],onPress:this.setFormaldehydeLevel},
        {name:'PM报警级别',value:this.warnLevel[alarm_pm],onPress:this.setPMLevel},
        {name:'自动待机时间',value:this.standbyTimes[standbyTime-1],onPress:this.setStandbyTime},
        {name:'自动检测时间',value:this.detectionTimes[detectionTime-1],onPress:this.setDetectionTime},
        {name:'报警声音开关',alarmVoice},
        {name:'设备校时',value:'',onPress:this.setTime}
      ]
    ]
    console.log({light})
    return (
      <View style={styles.container}>
        <Header back={() => navigation.goBack()} title={'设置'} />
        {this.deviceType==2?<View style={styles.top}>
          <Image source={require('../images/set/back.png')} style={{width:pxToDp(750),height:pxToDp(371),position:'absolute',left:pxToDp(0),top:pxToDp(0)}} />
          <Image source={require('../images/set/quansan.png')} style={{width:pxToDp(208),height:pxToDp(208),marginTop:pxToDp(23)}} />
          <View style={{width:pxToDp(750),height:pxToDp(56),flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginTop:pxToDp(49)}} >
            <Image source={require('../images/set/low.png')} style={{ width:pxToDp(56),height:pxToDp(56),left:pxToDp(27) }} />
            <Slider
              style={styles.slider}
              step={1}
              minimumValue={1}
              maximumValue={10}
              minimumTrackTintColor={'#3FABFC'}
              maximumTrackTintColor={'#6D9BC2'}
              trackStyle={styles.trackStyle}
              thumbStyle={styles.thumbStyle}
              value={light||1}
              onSlidingComplete={value => this.setLightLevel(value)}
            />
            <Image source={require('../images/set/height.png')} style={{ width:pxToDp(56),height:pxToDp(56),right:pxToDp(29) }} />
          </View>
        </View>:null}
        {this.deviceType==1?<Image source={require('../images/set/quanshan.png')} style={{width:pxToDp(750),height:pxToDp(340)}} />:null}
        {lines[this.deviceType-1].map((line,index)=>this._renderItem(line,index))}
        <Mask show={this.state.picker} />
      </View>
    )
  }
}

const marginTop = getStatusBarHeight(true)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBFBFB'
  },
  top:{
    width:pxToDp(750),
    height:pxToDp(371),
    alignItems: 'center'
  },
  slider: {
    width: pxToDp(540)
  },
  trackStyle: {
    height: pxToDp(10),
    borderRadius: pxToDp(5)
  },
  thumbStyle: {
    backgroundColor: '#fff',
    width: pxToDp(56),
    height: pxToDp(56),
    borderRadius: pxToDp(56 / 2),
    borderColor: '#3FABFC',
    borderWidth: pxToDp(3)
  },
  lineBox: {
    width:pxToDp(750),
    height:pxToDp(level()*10+90),
    flexDirection:'row',
    alignItems:'center',
    backgroundColor: '#fff'
  },
  leftText:{
    fontSize:pxToDp(30),
    color:'#333',
    position: 'absolute',
    left:pxToDp(37)
  },
  rightIcon:{
    width:pxToDp(16),
    height:pxToDp(29)
  },
  line: {
    height:1,
    width:pxToDp(750),
    backgroundColor:'#D8D8D8',
    opacity:0.5783
  },
  rightText: {
    position:'absolute',
    right:pxToDp(67),
    fontSize:pxToDp(30),
    color:'#333',
  },
  switch:{
    position:'absolute',
    right:pxToDp(33),
  }
})
const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SetDevice)
