import React from 'react'
import {
  ScrollView,
  View,
  StyleSheet,
  Text,
  ListView,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StatusBar
} from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import { SwipeListView } from 'react-native-swipe-list-view'
import moment from 'moment'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/userAction'

class Message extends React.Component {
  constructor (props) {
    super(props)
  }
  componentDidMount () {
    const { navigation, actions, publicReducer } = this.props
    this._didFocusSubscription =  navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content', true)
      StatusBar.setBackgroundColor('transparent')
      let body = {
        currentPage: 1,
        currentSize: 100,
        uid: publicReducer.myInfo.uid,
        city: publicReducer.city
      }
      actions.getMessages(body)
    })
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }
  cutString (str, len) {
    if (str.length * 2 <= len) {
      return str
    }
    let strlen = 0
    let s = ''
    for (let i = 0; i < str.length; i++) {
      s = s + str.charAt(i)
      if (str.charCodeAt(i) > 128) {
        strlen = strlen + 2
        if (strlen >= len) {
          return s.substring(0, s.length - 1) + '...'
        }
      } else {
        strlen = strlen + 1
        if (strlen >= len) {
          return s.substring(0, s.length - 2) + '...'
        }
      }
    }
    return s
  }

  _renderItem = (item) => {
    const { messageTitle, createTime, messageContent } = item
    return (
      <View style={styles.itemWrapper}>
        <View style={styles.iconWrapper}>
          <Image style={styles.icon} source={require('../images/notice.png')} />
        </View>
        <View style={styles.msgWrapper}>
          <View style={styles.itemRow}>
            <Text numberOfLines={1} style={styles.title}>{this.cutString(messageTitle, 15)}</Text>
            <Text style={styles.grayText}>{createTime}</Text>
          </View>
          <Text numberOfLines={2} style={styles.content}>{this.cutString(messageContent, 60)}</Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('MessageDetails', {msg: item})}>
            <Text style={styles.linkText}>查看详情</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  render () {
    const { navigation, publicReducer } = this.props
    const { messages } = publicReducer
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.goBack()}
          title={'我的消息'}
        />
        {
          messages.length === 0
            ? <View style={styles.msgPicBox}>
              <Image style={styles.msgPic} source={require('../images/xx-7.png')} />
              <Text>暂无消息</Text>
            </View>
            : <ScrollView style={styles.messageBox} >
              <SwipeListView
                closeOnRowPress={true}
                closeOnScroll={true}
                dataSource={ds.cloneWithRows(messages)}
                renderRow={data => this._renderItem(data)}
                // renderHiddenRow={(data, secdId, rowId, rowMap) => (
                //   <TouchableOpacity onPress={() => { rowMap[`${secdId}${rowId}`].closeRow() }} style={{flex:1,zIndex: 0}} >
                //     <View style={styles.deleteMenu}>
                //       <Text style={styles.deleteText}>删除</Text>
                //     </View>
                //   </TouchableOpacity>
                // )}
                disableRightSwipe
                enableEmptySections
                // rightOpenValue={pxToDp(-176)}
              />
            </ScrollView>
        }

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    opacity:1
  },
  messageBox: {
    flex: 1,
    marginTop: pxToDp(30)
  },
  msgPicBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  msgPic: {
    width: pxToDp(120),
    height: pxToDp(104),
    marginTop:pxToDp(-100)
  },
  msgBox: {
    flexDirection: 'row',
    paddingHorizontal: pxToDp(30),
    alignItems: 'center',
    backgroundColor: '#fff',
    height: pxToDp(176),
    paddingTop: pxToDp(30),
    paddingBottom: pxToDp(10)
  },
  voice: {
    width: pxToDp(100),
    height: pxToDp(100),
    position: 'absolute',
    top: pxToDp(-20),
    left: pxToDp(0)
  },
  leftImage: {
    width: pxToDp(100),
    height: pxToDp(100),
    marginRight: pxToDp(20)
  },
  msgText: {
    flex: 1,
    borderBottomColor: '#eee',
    borderBottomWidth: pxToDp(2)
  },
  msgTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  msgTime: {
    fontSize: pxToDp(24),
    color: '#999999'
  },
  title: {
    fontSize: pxToDp(32),
    color: '#333'
  },
  contStyle: {
    fontSize: pxToDp(28),
    marginTop: pxToDp(10),
    color: '#666'
  },
  detailsBox: {
    width: pxToDp(100),
    position: 'absolute',
    right: 0,
    bottom: pxToDp(20)
  },
  details: {
    color: '#3FABFC',
    fontSize: pxToDp(24),
    lineHeight: pxToDp(40)
  },
  deleteMenu: {
    backgroundColor: '#E93232',
    width: pxToDp(176),
    height: pxToDp(176),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 0,
    bottom: 0
  },
  deleteText: {
    color: '#fff',
    fontSize: pxToDp(32)
  },
  itemWrapper: {
    backgroundColor: '#FFF',
    paddingHorizontal: pxToDp(30),
    paddingTop: pxToDp(30),
    flexDirection: 'row'
  },
  msgWrapper: {
    flex: 1,
    paddingBottom: pxToDp(30),
    borderBottomWidth: pxToDp(2),
    borderBottomColor: '#eee',
  },
  iconWrapper: {
    width: pxToDp(100),
    height: pxToDp(100),
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: pxToDp(30)
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  linkText: {
    fontSize: pxToDp(24),
    color: '#3FABFC',
    textAlign: 'right'
  },
  grayText: {
    fontSize: pxToDp(24),
    color: '#999'
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Message)
