import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, Image, TouchableOpacity, Text, StatusBar, NativeModules } from 'react-native'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import store from 'react-native-simple-store'
import Toast from 'react-native-root-toast'
import { NetworkInfo } from 'react-native-network-info'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

import Geolocation from 'Geolocation'

class NetWork extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      wifi:''
    }
  }
  componentDidMount () {
    NetworkInfo.getSSID(wifi => {
      this.setState({wifi})
    })

    this._didFocusSubscription =  this.props.navigation.addListener('didFocus', () => {
      if(!this.state.address){
        this.getAddress()
      }
      StatusBar.setBarStyle('dark-content', true)
      StatusBar.setBackgroundColor('transparent')
    })
    this._willBlueSubscription =  this.props.navigation.addListener('willBlur', () => {
      StatusBar.setBarStyle('light-content', true)
      this.toast && Toast.hide(this.toast)
      this.props.actions.hideLoading()
      NativeModules.EspTouchModule.clear()
    })
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }
  getMac = (callback) =>{
    this.happyToast('正在获取设备标识')
    NativeModules.EspTouchModule.start().then((info)=>{
      info = info.split('=')[1].split(':')[1]
      let mac = info.slice(0,info.length-1)
      this.state.mac = mac
      callback()
    }).catch(err=>{
      this.happyToast('设备标识获取失败',true)
      console.log(err)
    })
  }
  key = true
  onSubmit = () => {
    this.props.actions.showLoading()
    // this.measureLayout()
    this.deviceNetwork()
    // this.pair()
  }
  pair = () => {
    const {wifi,password} = this.state
    let config = {
      type: 'esptouch',
      ssid: wifi,
      bssid: '',
      password: password,
      timeout: 60000
    }
    console.log(config)
    Smartconfig.start(config).then((res)=>{
      console.log(res)
    }).catch((error)=>{
      console.log(error,123)
    })
  }
  toast
  happyToast = (message,restart=false) => {
    this.toast && Toast.hide(this.toast)
    const that = this
    this.toast = Toast.show(message, {
      duration: restart?3000:600000,
      position: 0,
      shadow: true,
      shadowColor:'#F4F4F4',
      backgroundColor:'#F4F4F4',
      textColor:'#333',
      animation: true,
      hideOnPress: true,
      delay: 0,
    })
    if(restart) this.props.actions.hideLoading()
  }
  deviceNetwork(){
    this.happyToast('正在给设备联网')
    const { wifi,password } = this.state
    NativeModules.EspTouchModule.setNetwork(wifi,password,'1').then(res=>{
      if(res === "Esptouch fail"){
        this.happyToast('设备联网失败，请重新开始',true)
      }else{
        if(this.state.mac){
          this.bindDevice()
        }else{
          this.getMac(this.bindDevice)
        }
      }
    })
  }
  bindDevice = () => {
    const { actions, publicReducer } =this.props
    const {uid,address, mac} = this.state
    const { city } = publicReducer
    if(!address){
      this.happyToast('未开启定位权限，无法获取当前位置，请在开启权限后重新启动APP再试',true)
      return
    }
    console.log(this.state)
    if(!mac){
      this.happyToast('设备标识获取失败，重新启动APP再试',true)
    }
    if(uid&&city&&address&&mac){
      actions.addDevice({uid,city,address,mac}).then((res)=>{
        this.happyToast('设备绑定成功!',true)
        this.state.mac = ''
      }).catch(err=>{
        this.happyToast(err.message||'设备绑定失败!',true)
      })
    }
  }

  getAddress = () => {
    Geolocation.getCurrentPosition(
      location => {
        const {latitude,longitude} = location.coords
        this.state.address = `${latitude},${longitude}`
        // this.happyToast(this.state.address)
      },
      error => {
        this.happyToast('无法获取当前位置信息')
      }
    )
  }
  render () {
    const { navigation, publicReducer } = this.props
    const { wifi, password } = this.state
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} keyboardShouldPersistTaps={'handled'} >
          <TouchableOpacity style={{width:pxToDp(30),height:pxToDp(30),position:'absolute',right:pxToDp(40),top:pxToDp(marginTop+35)}} onPress={()=>{navigation.goBack()}} >
            <Image style={{width:pxToDp(30),height:pxToDp(30)}} source={require('../images/close.png')} />
          </TouchableOpacity>
          <View style={styles.logo}>
            <Image style={styles.logoTop} source={require('../images/logo1.png')} />
            <Image style={styles.logoBottom} source={require('../images/logoTextLogin.png')} />
          </View>
          <View style={styles.inputWrapper}>
            <Image style={{width:pxToDp(56),height:pxToDp(38),marginLeft:pxToDp(-10),marginRight:pxToDp(8)}} source={require('../images/wifi.png')} />
            <TextInput
              style={styles.TextInputBox}
              underlineColorAndroid='transparent'
              placeholder='WIFI账号'
              placeholderTextColor='#999999'
              onChangeText={(text) => this.setState({ wifi: text })}
              value={wifi}
            />
          </View>
          <View style={[styles.inputWrapper, { marginBottom: pxToDp(60) }]}>
            <Image style={styles.userIcon} source={require('../images/password.png')} />
            <TextInput
              ref='passwordInLogin'
              style={styles.TextInputBox}
              placeholder='WIFI密码'
              secureTextEntry
              underlineColorAndroid='transparent'
              placeholderTextColor='#999999'
              returnKeyLabel='绑定'
              onChangeText={(text) => this.setState({ password: text })}
              value={password}
              returnKeyType='send'
              onSubmitEditing={this.onSubmit}
            />
          </View>
          <Button
            title='一键绑定'
            disabled={publicReducer.loading}
            buttonStyle={styles.buttonStyle}
            onPress={this.onSubmit}
          />
          <Text style={[styles.tipText,{marginTop:pxToDp(50)}]}>温馨提示：</Text>
          <Text style={styles.tipText}>1.请先确保您的手机连上WIFI</Text>
          <Text style={styles.tipText}>2.正确输入您的WIFI密码</Text>
          <Text style={styles.tipText}>3.打开设备的配对模式</Text>
          <Text style={styles.tipText}>4.点击一键绑定按钮，并进行等待，请不要进行任何操作</Text>
          <Text style={styles.tipText}>5.如果提示失败，请重新从步骤1开始</Text>
        </ScrollView>
        {/*<Loading show={publicReducer.loading} />*/}
      </View>
    )
  }
}
const marginTop = getStatusBarHeight(true)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: pxToDp(150)
  },
  logoTop: {
    width: pxToDp(190),
    height: pxToDp(120),
    resizeMode: 'contain'
  },
  logoBottom: {
    width: pxToDp(140),
    height: pxToDp(50),
    resizeMode: 'contain',
    marginBottom: pxToDp(90)
  },
  inputWrapper: {
    flexDirection: 'row',
    height: pxToDp(100),
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#ddd',
    borderRadius: pxToDp(50),
    paddingHorizontal: pxToDp(42),
    marginHorizontal: pxToDp(30),
    marginTop: pxToDp(30)
  },
  userIcon: {
    width: pxToDp(40),
    height: pxToDp(46),
    marginRight: pxToDp(14)
  },
  TextInputBox: {
    flex: 1,
    paddingVertical: 0,
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    marginHorizontal: pxToDp(30)
  },
  bottomTip: {

  },
  tipText: {
    color:'#666666',
    fontSize:pxToDp(30),
    marginLeft: pxToDp(75),
    marginRight: pxToDp(75),
    lineHeight:pxToDp(50)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NetWork)
