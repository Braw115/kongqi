import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, TouchableOpacity, Text } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import store from 'react-native-simple-store'
import Loading from '../components/Loading'
import { check } from '../utils/function'
import { NavigationActions } from 'react-navigation'
import Toast from 'react-native-toast'

const Verify = (props) => (
  <TouchableOpacity disabled={!props.verify} style={props.verify ? styles.codeOn : styles.codeOff} onPress={props.onPress} >
    <Text style={props.verify ? styles.codeTextOn : styles.codeTextOff}>{props.verifyText}</Text>
  </TouchableOpacity>
)
class PhoneChange extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      phone: '',
      uid: '',
      code: '',
      codeTime: 0,
      nowTime: 0
    }
  }

  componentDidMount () {
    store.get('user').then(res => {
      console.log(res)
      this.setState({ uid: res.uid })
    }).catch()
    this._didFocusSubscription =  this.props.navigation.addListener('didFocus', () => {
      //设置倒计时
      store.get('codeTime').then((codeTime) => {
        this.setState({
          codeTime: codeTime,
          nowTime: new Date().getTime()
        })
        this.interval = setInterval(() => {
          let nowTime = new Date().getTime()
          if (nowTime > codeTime) {
            this.interval && clearInterval(this.interval)
          } else {
            this.setState({nowTime: nowTime})
          }
        }, 1000)
      }).catch()
    })
  }

  getCode = () => {
    const phone = this.state.phone
    const { actions, publicReducer } = this.props
    let oldPhone = publicReducer.myInfo.phone
    if (check.Phone(phone)) {
      if ( oldPhone === phone ) {
        Toast.showShortBottom('请输入新手机号')
        return false
      }
      let body = {phone}
      actions.sendCode(body).then((res) => {
        let finishTime = new Date().getTime() + 60000
        store.save('codeTime', {finishTime})
        this.setState({
          codeTime: finishTime
        })
        this.interval = setInterval(() => {
          let nowTime = new Date().getTime()
          if ( nowTime > finishTime ) {
            this.interval && clearInterval(this.interval)
          } else {
            this.setState({ nowTime: nowTime })
          }
        }, 1000)
      }).catch()
    }
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
    this.interval && clearInterval(this.interval)
  }
  onSubmit = () => {
    const { actions, navigation, publicReducer } = this.props
    const { phone, uid, code } = this.state
    let body = { phone, uid, code }
    let oldPhone = publicReducer.myInfo.phone
    if( check.Phone(phone) && check.Code(code) ){
      if ( oldPhone === phone ) {
        Toast.showShortBottom('请输入新手机号')
        return false
      }
      actions.modifyPhone(body).then(res => {
        navigation.reset([
          NavigationActions.navigate({routeName: 'Splash',params:{unInit:true}}),
          NavigationActions.navigate({routeName: 'Login'})
        ],1)
      }).catch()
    }
  }
  render () {
    const { navigation, publicReducer } = this.props
    const { codeTime, nowTime } = this.state
    const verify = codeTime > nowTime ? false: true // false 表示不能发送验证码
    const verifyText = verify ? '获取验证码' : Math.ceil((codeTime - new Date().getTime()) / 1000) + 's'
    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.navigate('PersonInfo')}
          title={'修改手机号'}
        />
        <ScrollView style={styles.container} >
          <TextInput
            style={[styles.TextInputBox, { marginTop: pxToDp(280) }]}
            placeholder='新的手机号'
            underlineColorAndroid='transparent'
            placeholderTextColor='#999999'
            onChangeText={(text) => this.setState({ phone: text })}
            maxLength={11}
          />
          <View style={styles.TextInputThree}>
            <TextInput
              style={[styles.TextInputBox, { flex: 1, marginHorizontal: 0 }]}
              placeholder='请输入验证码'
              underlineColorAndroid='transparent'
              placeholderTextColor='#999999'
              onChangeText={(text) => this.setState({ code: text })}
              maxLength={4}
            />
            <Verify onPress={this.getCode} verifyText={verifyText} verify={verify} />
          </View>
          <Button
            title='确定'
            buttonStyle={styles.buttonStyle}
            onPress={this.onSubmit}
          />
        </ScrollView>
        <Loading show={publicReducer.loading} text={'提交中'} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  TextInputBox: {
    height: pxToDp(80),
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#d8d8d8',
    fontSize: pxToDp(32),
    alignItems: 'center',
    paddingVertical: 0,
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(30)
  },
  TextInputThree: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(60),
    marginBottom: pxToDp(150)
  },
  code: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#dddddd'
  },
  codeText: {
    color: '#999999',
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    marginHorizontal: pxToDp(30)
  },
  codeOff: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#dddddd'
  },
  codeOn: {
    width: pxToDp(220),
    height: pxToDp(80),
    borderWidth: StyleSheet.hairlineWidth * 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: pxToDp(40),
    borderColor: '#DDDDDD',
    backgroundColor: '#3FABFC'
  },
  codeTextOff: {
    color: '#999',
    fontSize: pxToDp(32)
  },
  codeTextOn: {
    color: '#fff',
    fontSize: pxToDp(32)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhoneChange)
