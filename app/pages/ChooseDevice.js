import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, TouchableOpacity, Text } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import Toast from 'react-native-toast'
import Loading from '../components/Loading'
import {check} from '../utils/function'
import store from 'react-native-simple-store'
import moment from 'moment'

class ChooseDevice extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      device: '13016414219',
    }
  }

  componentDidMount () {
    const { navigation, publicReducer, actions } =this.props
    this._didFocusSubscription =  navigation.addListener('didFocus', () => {
      const { myInfo } = publicReducer
      actions.queryDeviceList( myInfo )

    })
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }
  chooseDevice = mac => {
    const { actions, navigation } = this.props
    actions.chooseDevice(mac)
    navigation.goBack()
  }
  render () {
    const { navigation, publicReducer } =this.props
    const { devices, defaultDevice } = publicReducer
    // let deviceNames = devices.map(item=>{
    //   let arr = item.mac.split(" ")
    //   let newArr = arr.map((a)=>{
    //       return a.length===1 ? `${a} `: a
    //   })
    //   return newArr.join(' ')
    // })
    const deviceNames = devices.map(item=>item.mac)

    return (
      <View style={styles.container}>
        <Header back={() => navigation.goBack()} title={'选择设备'} />
        <Text style={styles.title}>设备MAC地址</Text>
        <ScrollView style={styles.deviceBox} >
          {deviceNames.map((name,i) => {
            return (
              <TouchableOpacity style={styles.deviceLine} key={i} onPress={()=>this.chooseDevice(name)}>
                <Text style={styles.deviceName}>{name}</Text>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  title: {
    color: '#666',
    backgroundColor: '#EEE',
    height: pxToDp(70),
    borderColor: '#999',
    borderWidth: pxToDp(1),
    lineHeight: pxToDp(60),
    paddingLeft: pxToDp(30)
  },
  deviceBox: {
    flex: 1,
  },
  deviceName: {
    lineHeight: pxToDp(70),
    fontSize: pxToDp(32),
    color: '#333'
  },
  deviceLine: {
    marginHorizontal: pxToDp(36),
    height: pxToDp(70),
    flex: 1,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderColor: '#eee'
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChooseDevice)
