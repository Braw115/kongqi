import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, BackHandler, TouchableOpacity, Text, NativeModules } from 'react-native'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import Toast from 'react-native-toast'
import Loading from '../components/Loading'
import Header from '../components/Header/Header'
var Geolocation = require('Geolocation')
import { NetworkInfo } from 'react-native-network-info'

class BindDevice extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      uid: props.publicReducer.myInfo.uid,
      mac: '',
      address: '',
      wifi: '',
      password: 'szlansoo',
      info:''
    }
  }
  componentDidMount(){
    const {setNetwork,start} = NativeModules.EspTouchModule
    NetworkInfo.getSSID(wifi => {
      this.setState({wifi})
    });
    start()
    // setNetwork('lansoo-1806','szlansoo','1')
    //   .then(res=>console.log(res))
    //   .catch(err=>console.log(err))
  }
  onSubmit = () => {
    const { actions } = this.props
    let body = {...this.state}
    if (!body.mac.trim()) {
      Toast.showShortCenter("请输入设备标签上的MAC地址")
      return
    }
    actions.addDevice(body).then(res=>{
      this.setState({mac:''})
    }).catch()
  }
  unbind = (item) => {
    const { actions } = this.props
    let body = { uid: this.state.uid, mac: item.mac}
    actions.unBindDevice(body)
  }
  getAddress = () => {
    Geolocation.getCurrentPosition(
      location => {
        const {latitude,longitude} = location.coords
        this.state.address = `${latitude},${longitude}`
      },
      error => {
        console.log(error)
      }
    )
  }
  backEvent = () => {
    const { navigation, publicReducer } = this.props
    const { devices } = publicReducer
    if(devices.length === 0){
      Toast.showLongCenter('您需要先绑定一台设备才能开始使用')
    }else{
      navigation.navigate('Home')
    }
  }
  Test(){
    console.log(this.state)
    this.measureLayout()
  }
  async  measureLayout() {
    const {setNetwork,clear} = NativeModules.EspTouchModule
    const { wifi,password } = this.state
    try {
      var res = await setNetwork(wifi,password,'1')
      this.setState({info:JSON.stringify(res)})
      console.log(res,123)
      this.setTheMode()
    } catch (e) {
      this.setState({info:JSON.stringify(e)})
      console.log(e,456)
    }
  }
  async  setTheMode() {
    const {setMode} = NativeModules.EspTouchModule
    try {
      var res = await setMode()
      this.setState({info:this.state.info+JSON.stringify(res)})
      console.log(res)
    } catch (e) {
      this.setState({info:this.state.info+JSON.stringify(e)})
      console.log(e)
    }
  }

  render () {
    const { publicReducer } = this.props
    const { mac,wifi,password,info } = this.state
    const { devices } = publicReducer
    return (
      <View style={styles.container}>
        <Header
          back={() => this.backEvent()}
          title={'设备绑定'}
        />
        <View style={styles.line} />
        {/*<View style={styles.addBox}>*/}
          {/*<TextInput*/}
            {/*style={styles.TextInputBox}*/}
            {/*placeholder='请输入设备标签上的mac地址'*/}
            {/*underlineColorAndroid='transparent'*/}
            {/*placeholderTextColor='#999999'*/}
            {/*onChange={()=>this.getAddress()}*/}
            {/*onChangeText={(text) => this.setState({ mac: text })}*/}
            {/*value={mac}*/}
          {/*/>*/}
          {/*<Button*/}
            {/*title='绑定'*/}
            {/*buttonStyle={styles.buttonStyle}*/}
            {/*onPress={this.onSubmit}*/}
          {/*/>*/}
        {/*</View>*/}
        {/*<ScrollView style={styles.container} >*/}
          {/*{ devices.map((item, index) => {*/}
            {/*return (*/}
              {/*<View style={styles.macLine} key={index}>*/}
                {/*<Text style={[styles.date, styles.dateFont]}>{item.mac}</Text>*/}
                {/*<TouchableOpacity style={styles.box} onPress={()=>this.unbind(item)} >*/}
                  {/*<Text style={styles.unbind}>解绑</Text>*/}
                {/*</TouchableOpacity>*/}
              {/*</View>*/}
            {/*)*/}
          {/*})}*/}
        {/*</ScrollView>*/}
        <Loading show={publicReducer.loading} text={'提交中'} />

          <Text style={{fontSize:pxToDp(32),marginVertical:pxToDp(30),marginHorizontal:pxToDp(60)}}>WIFI名称：{wifi}</Text>
          <TextInput
            style={styles.TextInputBox}
            placeholder='请输入WIFI密码'
            underlineColorAndroid='transparent'
            placeholderTextColor='#999999'
            onChange={()=>this.getAddress()}
            onChangeText={(text) => this.setState({ password: text })}
            value={password}
          />
          <TouchableOpacity style={{width:pxToDp(690),height:pxToDp(100),backgroundColor:'#3FABFC',justifyContent:'center',alignItems:'center',borderRadius:pxToDp(50),margin:pxToDp(30)}}
                            onPress={()=>this.Test()}
          >
            <Text style={{fontSize:pxToDp(36),color:'#fff'}}>开始绑定</Text>
          </TouchableOpacity>
        <Text>{info}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  line: {
    borderBottomColor: '#D8D8D8',
    borderBottomWidth: 1,
    marginTop: pxToDp(20)
  },
  addBox: {
    width: pxToDp(750),
    height: pxToDp(100),
    marginTop: pxToDp(50),
    marginBottom: pxToDp(93),
    alignItems: 'center',
    flexDirection: 'row'
  },
  TextInputBox: {
    width: pxToDp(500),
    marginLeft: pxToDp(58),
    color: '#999',
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    backgroundColor: '#3FABFC',
    width: pxToDp(145),
    height: pxToDp(72),
    borderRadius: pxToDp(50),
    marginRight: pxToDp(30)
  },
  unbind: {
    color: '#FF4747',
    fontSize: pxToDp(32)
  },
  macLine: {
    height: pxToDp(60),
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: pxToDp(60),
    marginRight: pxToDp(35)
  },
  box: {
    position: 'absolute',
    right: pxToDp(30)
  },
  dateFont: {
    fontSize: pxToDp(32),
    color: '#333'
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BindDevice)
