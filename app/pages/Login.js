import React from 'react'
import { ScrollView, View, StyleSheet, TextInput, Image, TouchableOpacity, Text, StatusBar } from 'react-native'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import Loading from '../components/Loading'
import { check } from '../utils/function'
import store from 'react-native-simple-store'
import { NavigationActions } from 'react-navigation'

class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }
  }
  componentDidMount () {
    store.get('phone').then((res) => {
      this.setState({username: res})
    })
    this._didFocusSubscription =  this.props.navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('dark-content', true)
      StatusBar.setBackgroundColor('transparent')
    })
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }
  onSubmit = () => {
    const { actions, navigation } = this.props
    const { username, password } = this.state
    if (check.Phone(username) && check.Password(password)) {
      let body = {
        username: username,
        password: password
      }
      actions.login(body).then(()=>{
        console.log('登录成功准备跳转')
        navigation.reset([
          NavigationActions.navigate({routeName: 'Splash',params:{unInit:true}}),
          NavigationActions.navigate({routeName: 'TabNavigator'})
        ],1)
      })
    }
  }

  render () {
    const { navigation, publicReducer } = this.props
    const { username, password } = this.state
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} keyboardShouldPersistTaps={'handled'} >
          <View style={styles.logo}>
            <Image style={styles.logoTop} source={require('../images/logo1.png')} />
            <Image style={styles.logoBottom} source={require('../images/logoTextLogin.png')} />
          </View>
          <View style={styles.inputWrapper}>
            <Image style={styles.userIcon} source={require('../images/account.png')} />
            <TextInput
              style={styles.TextInputBox}
              underlineColorAndroid='transparent'
              placeholder='手机号'
              placeholderTextColor='#999999'
              onChangeText={(text) => this.setState({ username: text })}
              value={username}
              onEndEditing={() => { check.Phone(username) }}
              keyboardType='numeric'
              maxLength={11}
            />
          </View>
          <View style={[styles.inputWrapper, { marginBottom: pxToDp(60) }]}>
            <Image style={styles.userIcon} source={require('../images/password.png')} />
            <TextInput
              ref='passwordInLogin'
              style={styles.TextInputBox}
              placeholder='请输入密码'
              secureTextEntry
              underlineColorAndroid='transparent'
              placeholderTextColor='#999999'
              returnKeyLabel='登录'
              onChangeText={(text) => this.setState({ password: text })}
              value={password}
              returnKeyType='send'
              onSubmitEditing={this.onSubmit}
            />
          </View>
          <Button
            title='登录'
            buttonStyle={styles.buttonStyle}
            onPress={this.onSubmit}
          />
          <View style={styles.bottomTip}>
            <TouchableOpacity onPress={() => { navigation.navigate('Register') }}>
              <Text style={styles.tipText}>注册</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { navigation.navigate('PasswordChange') }}>
              <Text style={styles.tipText}>忘记密码</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Loading show={false || publicReducer.loading} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: pxToDp(200)
  },
  logoTop: {
    width: pxToDp(190),
    height: pxToDp(120),
    resizeMode: 'contain'
  },
  logoBottom: {
    width: pxToDp(140),
    height: pxToDp(50),
    resizeMode: 'contain',
    marginBottom: pxToDp(90)
  },
  inputWrapper: {
    flexDirection: 'row',
    height: pxToDp(100),
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#ddd',
    borderRadius: pxToDp(50),
    paddingHorizontal: pxToDp(42),
    marginHorizontal: pxToDp(30),
    marginTop: pxToDp(30)
  },
  userIcon: {
    width: pxToDp(40),
    height: pxToDp(46),
    marginRight: pxToDp(14)
  },
  TextInputBox: {
    flex: 1,
    paddingVertical: 0,
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    marginHorizontal: pxToDp(30)
  },
  bottomTip: {
    flexDirection: 'row',
    marginHorizontal: pxToDp(100),
    justifyContent: 'space-between',
    marginTop: pxToDp(30)
  },
  tipText: {
    color: '#3FABFC',
    fontSize: pxToDp(32)
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
