import React from 'react'
import { StatusBar, View, StyleSheet, TouchableOpacity, Text, Image } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'
import Button from '../components/Button'
import NameModal from '../components/NameModal'
import SexModal from '../components/SexModal'
import store from 'react-native-simple-store'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'
import Loading from '../components/Loading'
import { NavigationActions } from 'react-navigation'
import Picker from 'react-native-picker'

class PersonInfo extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      tipVisible: false, // 昵称的模态框
      sexVisible: false,
      text: '',
      name: '',
      phone: '',
      sex: 0
    }
    console.log(this.props)
  }
  
  componentDidMount () {
    store.get('user').then(res => {
      this.setState({ name: res.name, phone: res.phone, sex: res.sex })
    }).catch((err) => {})
    this._willBlurSubscription =  this.props.navigation.addListener('willBlur', () => {
      Picker.hide()
    })
  }
  componentWillUnmount () {
    this._willBlurSubscription && this._willBlurSubscription.remove()
  }
  selectSex = () => {
    const sexs = ['男','女']
    const {publicReducer,actions} = this.props
    const {uid,sex = this.state.sex} = publicReducer.myInfo
    Picker.init({
      pickerData: sexs,
      selectedValue: [sexs[sex]],
      pickerTitleText:'选择性别',
      pickerConfirmBtnText:"确定",
      pickerCancelBtnText:"取消",
      onPickerConfirm: data => {
        let newSex = data[0] === sexs[0] ? 0 : 1
        if(newSex === sex){
          return
        }
        let body = { uid, sex: newSex }
        actions.modifyPersonInfo(body)
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      }
    })
    Picker.show()
  }
  modalCome = () => {
    StatusBar.setBarStyle('dark-content', true)
    StatusBar.setBackgroundColor('transparent')
    this.setState({ tipVisible: !this.state.tipVisible })
  }
  modalSexCome = () => {
    StatusBar.setBarStyle('dark-content', true)
    StatusBar.setBackgroundColor('transparent')
    this.setState({ sexVisible: !this.state.sexVisible })
  }

  logOut = () => {
    const { actions, navigation } = this.props
    actions.logOut().then(()=>{
      navigation.reset([
        NavigationActions.navigate({routeName: 'Splash',params:{unInit:true}}),
        NavigationActions.navigate({routeName: 'Login'})
      ],1)
    })

  }

  render () {
    const { navigation, publicReducer } = this.props
    const {myInfo} = publicReducer
    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.goBack()}
          title={'设置'}
        />
        <View style={[styles.infoLine, {marginTop: pxToDp(60)}]}>
          <Text style={styles.leftName}>昵称</Text>
          <TouchableOpacity
            style={styles.selectedCont}
            onPress={this.modalCome}
          >
            <Text style={styles.infoText}>{myInfo.name}</Text>
            <Image style={styles.rightPic} source={require('../images/3copy3.png')} />
          </TouchableOpacity>
        </View>
        <View style={styles.infoLine}>
          <Text style={styles.leftName}>性别</Text>
          <TouchableOpacity
            style={styles.selectedCont}
            onPress={()=>this.selectSex()}
          >
            <Text style={styles.infoText}>{myInfo.sex == 0 ? '男' : '女'}</Text>
            <Image style={styles.rightPic} source={require('../images/3copy3.png')} />
          </TouchableOpacity>
        </View>
        <View style={styles.infoLine}>
          <Text style={styles.leftName}>手机号</Text>
          <TouchableOpacity
            style={styles.selectedCont}
            onPress={() => { navigation.navigate('PhoneChange') }}
          >
            <Text style={styles.infoText}>{myInfo.phone}</Text>
            <Image style={styles.rightPic} source={require('../images/3copy3.png')} />
          </TouchableOpacity>
        </View>
        <View style={styles.infoLine}>
          <Text style={styles.leftName}>邮箱</Text>
          <TouchableOpacity
            style={styles.selectedCont}
            onPress={() => { navigation.navigate('MailBox',{mail:myInfo.mail}) }}
          >
            <Text style={styles.infoText}>{myInfo.mail||"去绑定"}</Text>
            <Image style={styles.rightPic} source={require('../images/3copy3.png')} />
          </TouchableOpacity>
        </View>
        <View style={[styles.infoLine, { marginBottom: pxToDp(300) }]}>
          <Text style={styles.leftName}>修改密码</Text>
          <TouchableOpacity
            style={styles.selectedCont}
            onPress={() => { navigation.navigate('PasswordInfoChange') }}
          >
            <Text style={styles.infoText}>点击修改</Text>
            <Image style={styles.rightPic} source={require('../images/3copy3.png')} />
          </TouchableOpacity>
        </View>
        <View style={styles.footer}>
          <Button title='退出登录' buttonStyle={styles.buttonStyle} onPress={this.logOut} />
          {
            this.state.tipVisible ? <NameModal parent={this} /> : null
          }
          {
            this.state.sexVisible ? <SexModal parent={this} sex={myInfo.sex} /> : null
          }
          <Loading show={publicReducer.loading} />
        </View>
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  infoLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: pxToDp(30),
    marginVertical: pxToDp(30)
  },
  leftName: {
    color: '#333',
    fontSize: pxToDp(32)
  },
  selectedCont: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightPic: {
    width: pxToDp(14),
    height: pxToDp(24)
  },
  infoText: {
    paddingRight: pxToDp(20),
    color: '#999',
    fontSize: pxToDp(32)
  },
  buttonStyle: {
    marginHorizontal: pxToDp(30),
    marginBottom: pxToDp(120)
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end'
  }
})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonInfo)
