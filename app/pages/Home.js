import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, StatusBar, BackHandler, Platform } from 'react-native'
import pxToDp from '../utils/pxToDp'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import LineChart from '../components/LineChart'
import HomeChartTitle from '../components/HomeChartTitle'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'



class Home extends React.Component {
  constructor (props) {
    super (props)
  }
  lastBackTime = 0
  componentDidMount () {
    // this.props.navigation.openDrawer()
    const { navigation, actions, publicReducer } = this.props

    this._didFocusSubscription =  navigation.addListener('willFocus', () => {
      StatusBar.setBarStyle('light-content', true)
      // if(this.props.publicReducer.isDrawOpen){
      //   navigation.openDrawer()
      // }
    })
    this._willBlueSubscription =  navigation.addListener('willBlur', () => {
      StatusBar.setBarStyle('dark-content', true)
      const state = this.props.navigation.dangerouslyGetParent().dangerouslyGetParent().state.isDrawerOpen
      actions.isDrawOpen(state)
    })
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
    this._willBlueSubscription && this._willBlueSubscription.remove()
  }

  render () {
    const { navigation, publicReducer } = this.props
    const { defaultDevice } = publicReducer
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <Image style={styles.topBg} source={require('../images/skyFine.png')} />
          <View style={styles.topRow}>
            <TouchableOpacity onPress={()=>navigation.openDrawer()}>
              <Image style={styles.people} source={require('../images/menu.png')} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { navigation.navigate('Message') }}>
              <Image style={styles.msg} source={require('../images/message.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.banner}>
            <View style={styles.middleBox}>
              <View style={styles.cycle}>
                <Image style={styles.circleLine} source={require('../images/quanquan.png')} />
                <Text style={styles.score}>30</Text>
                <Text style={styles.assess}>差</Text>
              </View>
              <Text style={styles.circleTextBottom}>略略略</Text>
            </View>
            <View style={styles.leftBox}>
              <Text style={styles.tempText}>27°C</Text>
              {/*<Image style={styles.tempPic} source={require('../images/duoyun.png')} />*/}
              {/*<Text style={styles.weather}>多云</Text>*/}
            </View>
            <View style={styles.rightBox}>
              <Text style={[styles.pm25, {marginTop: pxToDp(170)}]}>PM2.5</Text>
              <Text style={styles.pm25Value}><Text style={styles.value}>41</Text>ug/m3</Text>
              <Text style={styles.pm25}>PM10</Text>
              <Text style={styles.pm25Value}><Text style={styles.value}>52</Text>ug/m3</Text>
            </View>
          </View>
        </View>
        <View style={styles.foot}>
          <View style={styles.week}>
            <Image style={styles.weekLogo} source={require('../images/logo.png')} />
            <Text style={styles.title} >一周记录</Text>
            <TouchableOpacity style={styles.macBox} onPress={ () => navigation.navigate('ChooseDevice')} >
              <Text style={styles.macText}>{defaultDevice}</Text>
              <Image style={styles.dropImage} source={require('../images/drop.png')} />
            </TouchableOpacity>
          </View>
          <HomeChartTitle />
          <View style={styles.lineChartPic} pointerEvents='none'>
            <LineChart />
          </View>
        </View>
      </View >
    )
  }
}
const paddingTop = getStatusBarHeight(true)

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  top: {
    width: pxToDp(750),
    height: pxToDp(652)
  },
  topBg: {
    width: pxToDp(750),
    height: pxToDp(652),
    position: 'absolute',
    top: 0,
    left: 0
  },
  topRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: pxToDp(44),
    width: pxToDp(690),
    marginHorizontal: pxToDp(30),
    marginTop: paddingTop + pxToDp(10),
    position: 'absolute',
    top: 0
  },
  people: {
    width: pxToDp(44),
    height: pxToDp(44)
  },
  msg: {
    width: pxToDp(44),
    height: pxToDp(38),
    marginTop: pxToDp(5)
  },
  banner: {
    flexDirection: 'row',
    width: pxToDp(750),
    height: pxToDp(652),
    marginTop: pxToDp(20),
    marginBottom: pxToDp(106),
    justifyContent: 'center'
  },
  middleBox: {
    width: pxToDp(268),
    height: pxToDp(652)
  },
  cycle: {
    width: pxToDp(268),
    height: pxToDp(268),
    marginTop: pxToDp(150),
    justifyContent: 'center',
    alignItems: 'center'
  },
  bannerBoxLeft: {
    justifyContent: 'space-around',
    height: pxToDp(264),
    backgroundColor: 'blue'
  },
  temperature: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  tempPic: {
    width: pxToDp(80),
    height: pxToDp(62),
    marginTop: pxToDp(62)
  },
  weather: {
    fontSize: pxToDp(32),
    color: '#fff',
    marginTop: pxToDp(16)
  },
  circle: {
    height: pxToDp(264),
    justifyContent: 'center',
    alignItems: 'center'
  },
  circleLine: {
    width: pxToDp(268),
    height: pxToDp(268),
    position: 'absolute',
    left: 0,
    top: 0
  },
  score: {
    color: '#fff',
    fontSize: pxToDp(80)
  },
  assess: {
    color: '#fff',
    fontSize: pxToDp(36),
    fontWeight: '400'
  },
  circleTextBottom: {
    width: '100%',
    textAlign: 'center',
    color: '#fff',
    marginTop: pxToDp(60),
    fontSize: pxToDp(24)
  },
  leftBox: {
    position: 'absolute',
    left: pxToDp(40),
    top: 0,
    height: pxToDp(600),
    width: pxToDp(120),
    alignItems: 'center'
  },
  rightBox: {
    position: 'absolute',
    right: pxToDp(40),
    top: 0,
    height: pxToDp(600),
    width: pxToDp(150),
    alignItems: 'flex-start'
  },
  pm25: {
    marginTop: pxToDp(48),
    color: '#fff',
    fontSize: pxToDp(30)
  },
  pm25Value: {
    color: '#fff',
    fontSize: pxToDp(24)
  },
  value: {
    fontSize: pxToDp(48)
  },
  tempText: {
    marginTop: pxToDp(180),
    textAlign: 'center',
    color: '#fff',
    fontSize: pxToDp(48)
  },
  pmText: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  week: {
    height: pxToDp(100),
    borderColor: '#eeeeee',
    backgroundColor: '#fff',
    marginHorizontal: pxToDp(30),
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: pxToDp(1)
  },
  weekLogo: {
    width: pxToDp(43),
    height: pxToDp(27),
    marginRight: pxToDp(20)
  },
  lineChartPic: {
    height: pxToDp(380)
  },
  infoOutBox: {
    marginBottom: pxToDp(60)
  },
  infoTopLine: {
    height: pxToDp(20),
    backgroundColor: '#eee'
  },
  infomation: {
    backgroundColor: '#fff',
    height: pxToDp(100),
    paddingHorizontal: pxToDp(30),
    justifyContent: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderColor: '#eeeeee'
  },
  infoItem: {
    flexDirection: 'row',
    marginVertical: pxToDp(20)
  },
  infoText: {
    flex: 2,
    paddingHorizontal: pxToDp(30)
  },
  infoPicBox: {
    flex: 1
  },
  foot: {
    backgroundColor: '#fff',
    height: pxToDp(650),
    position: 'absolute',
    bottom: 0,
    left: 0
  },
  infoPic: {
    width: pxToDp(180),
    height: pxToDp(130)
  },
  fontWhite: {
    color: '#fff'
  },
  title: {
    color: '#666',
    fontSize: pxToDp(32)
  },
  macBox: {
    width: pxToDp(300),
    height: pxToDp(40),
    backgroundColor: '#fff',
    position: 'absolute',
    right: pxToDp(0),
    flexDirection: 'row',
    alignItems: 'center'
  },
  dropBox: {
    width: pxToDp(251),
    height: pxToDp(40),
    marginRight: pxToDp(14)
  },
  dropImage: {
    width: pxToDp(25),
    height: pxToDp(14)
  },
  macText: {
    color: '#999',
    fontSize: pxToDp(28),
    textAlign: 'right',
    marginRight:pxToDp(18)
  },
  macLine: {
    width: pxToDp(190),
    borderColor: '#eee',
    borderWidth: 2,
    borderRadius: 3
  },
  dropText: {
    fontSize: pxToDp(24)
  }
})
const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
