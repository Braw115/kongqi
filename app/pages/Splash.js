import React from 'react'
import { View, StyleSheet, Image, BackHandler, Platform, StatusBar } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/userAction'
import store from 'react-native-simple-store'
import pxToDp from '../utils/pxToDp'
import { tool } from '../utils/function'
import Toast from 'react-native-toast'
// import Toast from 'react-native-root-toast'
import JPushModule from 'jpush-react-native'
import DeviceInfo from 'react-native-device-info'

let Geolocation = require('Geolocation')

class Splash extends React.Component {
  constructor (props) {
    super(props)
    props.actions.saveVersion(DeviceInfo.getVersion())
  }
  timeOut = 5000
  componentDidMount () {
    const {navigation,actions} = this.props
    let unInit = navigation.getParam('unInit')
    !unInit && this.appInit() // 是否自动登录
    this.getCity()
    store.get('defaultDevice').then(res=>{
      if(res) actions.chooseDevice(res)
    }).catch()
    BackHandler.addEventListener('hardwareBackPress', () => {
      const{ routes } = navigation.dangerouslyGetParent().state
      actions.hideLoading()
      if (navigation) {
        if (routes.length == 2 ) {
          if (this.lastBackTime + 3000 >= Date.now()) {
            BackHandler.exitApp()
            return true
          }
          Toast.showShortBottom('再按一次退出应用')
          this.lastBackTime = Date.now()
        } else {
          const { routeName } = routes[routes.length-1]
          if(routeName === "BindDevice" && this.props.publicReducer.devices.length === 0){
            Toast.showLongCenter('您需要先绑定一台设备才能开始使用')
          }else{
            navigation.pop()
          }
        }
      }
      return true
    })
  }
  appInit = () => {
    const {navigation,actions} = this.props
    const ms = this.timeOut
    Promise.race([
      new Promise(function (resolve, reject) {
        store.get('user').then(res=>{
          if(res.token){
            actions.autoLogin().then((res)=>{
              store.get('defaultDevice').then(mac=>{
                actions.chooseDevice(mac)
                resolve(res)
              })
            })
          }
        }).catch(err=>{
          reject(err)
        })
      }),
      tool.timeOut(ms)
    ]).then(res => {
      if(res == 'autoLogin success'){
        navigation.navigate('TabNavigator')
      }
    }).catch(err=>{
      navigation.navigate('Login')
    })
  }
  getCity = () => {
    Geolocation.getCurrentPosition(
      location => {
        const { longitude, latitude } = location.coords
        let address  = `&location=${latitude},${longitude}`
        this.props.actions.getCity(address).then((city)=>{
          if(city){
            this.setJpush(city)
          }
        }).catch()
      },
      error => {
        // alert("获取位置失败："+ error)
      }
    );
  }
  setJpush = (city) => {
    JPushModule.clearAllNotifications()
    if (Platform.OS === 'android') {
      JPushModule.notifyJSDidLoad((resultCode) => {
      })
    }
    JPushModule.removeReceiveOpenNotificationListener()
    JPushModule.addReceiveOpenNotificationListener(map => {

    })
    JPushModule.cleanTags(success => {

      JPushModule.setTags([city],success=>{
        // console.log(success)
      })
    })
  }
  componentWillUnmount () {
    this.interval && clearInterval(this.interval)
    this._didFocusSubscription && this._didFocusSubscription.remove()
  }

  render () {
    const { navigation, publicReducer } = this.props
    return (
      <View style={styles.container}>
        <StatusBar barStyle={'dark-content'} />
        <Image style={styles.splash} source={require('../images/logoNew.png')} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  splash: {
    width: pxToDp(144),
    height: pxToDp(144),
    marginBottom: pxToDp(300)
  }

})

const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash)
