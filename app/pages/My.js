import React from 'react'
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  StatusBar,
  BackHandler,
  Platform,
  Alert,
  Linking
} from 'react-native'
import pxToDp from '../utils/pxToDp'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'
import {tool} from '../utils/function'
import Toast from 'react-native-toast'
import Header from '../components/Header/Header'
import _updateConfig from '../../update.json'
import { isFirstTime, isRolledBack, packageVersion, currentVersion, checkUpdate, downloadUpdate, switchVersion, switchVersionLater, markSuccess } from 'react-native-update'
const {appKey} = _updateConfig[Platform.OS]


class My extends React.Component {
  constructor (props) {
    super (props)
  }
  componentDidMount () {
    const {navigation} = this.props
    this._didFocusSubscription =  navigation.addListener('willFocus', () => {
      StatusBar.setBarStyle('dark-content', true)
    })
  }
  componentWillMount(){
    if (isFirstTime) {
      Alert.alert('提示', '这是当前版本第一次启动,是否要模拟启动失败?失败将回滚到上一版本', [
        {text: '是', onPress: ()=>{throw new Error('模拟启动失败,请重启应用')}},
        {text: '否', onPress: ()=>{markSuccess()}},
      ]);
    } else if (isRolledBack) {
      Alert.alert('提示', '刚刚更新失败了,版本被回滚.');
    }
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
    this._willBlueSubscription && this._willBlueSubscription.remove()
  }
  doUpdate = info => {
    downloadUpdate(info).then(hash => {
      Alert.alert('提示', '下载完毕,是否重启应用?', [
        {text: '是', onPress: ()=>{switchVersion(hash);}},
        {text: '否',},
        {text: '下次启动时', onPress: ()=>{switchVersionLater(hash);}},
      ]);
    }).catch(err => {
      Alert.alert('提示', '更新失败.');
    });
  };
  checkUpdate = () => {
    console.log(777)
    checkUpdate(appKey).then(info => {
      console.log(info)
      if (info.expired) {
        Alert.alert('提示', '您的应用版本已更新,请前往应用商店下载新的版本', [
          {text: '确定', onPress: ()=>{info.downloadUrl && Linking.openURL(info.downloadUrl)}},
        ]);
      } else if (info.upToDate) {
        Alert.alert('提示', '您的应用版本已是最新.');
      } else {
        Alert.alert('提示', '检查到新的版本'+info.name+',是否下载?\n'+ info.description, [
          {text: '是', onPress: ()=>{this.doUpdate(info)}},
          {text: '否',},
        ]);
      }
    }).catch(err => {
      Alert.alert('提示', '更新失败.');
    });
  };
  _renderItem = ({icon,name,tips,onPress},key) => {
    return (
      <View key={key}>
        <TouchableOpacity  style={styles.lineBox} onPress={onPress}>
          <Image source={icon} style={{width:pxToDp(50),height:pxToDp(50),position:'absolute',left:pxToDp(43)}} />
          <Text style={{position:'absolute',left:pxToDp(109),fontSize:pxToDp(32),color:'#666'}}>{name}</Text>
          <Text style={{position:'absolute',right:pxToDp(65),fontSize:pxToDp(28),color:'#666'}}>{tips}</Text>
          <Image source={require('../images/my/right.png')} style={{width:pxToDp(16),height:pxToDp(30),position:'absolute',right:pxToDp(32)}} />
        </TouchableOpacity>
        <View style={styles.line} />
      </View>
    )
  }
  navigate = name =>{
    this.props.navigation.navigate(name)
  }
  lines = [
    { icon: require('../images/my/message.png'), name: '我的消息', tips: '',onPress:()=>this.navigate('Message') },
    { icon: require('../images/my/set.png'), name: '设置', tips: '' ,onPress:()=>this.navigate('PersonInfo')},
    { icon: require('../images/my/version.png'), name: '版本检测', tips: `v${this.props.publicReducer.version}` ,onPress:this.checkUpdate},
    { icon: require('../images/my/aboutUs.png'), name: '关于我们', tips: '' ,onPress:()=>this.navigate('AboutUs')}
  ]

  render () {
    const { name,phone } = this.props.publicReducer.myInfo
    const newPhone = tool.hidePhone(phone)
    return (
      <View style={styles.container}>
        <Header title={'我的'} />
        <View style={styles.top}>
          <Image source={require('../images/my/headLike.png')} style={{width:pxToDp(95),height:pxToDp(95),marginTop:pxToDp(43)}} />
          <Text style={{fontSize:pxToDp(36),fontWeight:'600',color:'#666',marginTop:pxToDp(20)}}>{newPhone}</Text>
          <Text style={{fontSize:pxToDp(24),fontWeight:'400',color:'#666',marginTop:pxToDp(3)}}>{name}</Text>
        </View>
        <View style={styles.line} />
        {this.lines.map((line,index)=>this._renderItem(line,index))}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fBfBfB'
  },
  top: {
    width: pxToDp(750),
    height: pxToDp(314),
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  line: {
    height:1,
    width:pxToDp(750),
    backgroundColor:'#D8D8D8',
    opacity:0.5783
  },
  lineBox:{
    flexDirection:'row',
    height:pxToDp(109),
    width:pxToDp(750),
    justifyContent:'space-between',
    alignItems:'center',
    backgroundColor: '#fff',
  }

})
const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(My)
