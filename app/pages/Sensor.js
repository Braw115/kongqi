import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, StatusBar, ScrollView, TextInput,Dimensions } from 'react-native'
import pxToDp from '../utils/pxToDp'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'
import { bindActionCreators } from 'redux'
import * as userActions from '../actions/userAction'
import connect from 'react-redux/es/connect/connect'
import LinearGradient from 'react-native-linear-gradient'
import Echarts from 'native-echarts'
import moment from  'moment'
import Picker from 'react-native-picker'
import Toast from 'react-native-toast'
const {height,width} = Dimensions.get("window")
const level = () => {
  const k = height/width
  if(k>1.8)   return 3
  if(k>16/9)   return 2
  if(k>1.6)   return 1
}
const Mask = ({show=false}) => {
  return show?<View style={{flex:1,position:'absolute',left:0,top:0,backgroundColor:'rgba(40,43,49,1)',height:'100%',width:'100%',opacity:0.6017}} />
    :null
}
class Sensor extends React.Component {
  constructor (props) {
    super (props)
    this.state = {
      name: props.navigation.getParam ('name') || props.navigation.getParam ('mac'),
      edit: false,
      type: 0,
      aqi:50,
      picker:false
    }
  }
  componentDidMount () {
    const {navigation,actions,publicReducer} = this.props
    this._willFocusSubscription =  navigation.addListener('willFocus', () => {
      StatusBar.setBarStyle('light-content', true)
      const mac = navigation.getParam ('mac')
      actions.getDeviceStatus(mac)
      actions.getNowData(mac)
      actions.getWeekData(mac)
      actions.getOrderParam(mac)
    })
    // this._didFocusSubscription =  navigation.addListener('didFocus', () => {
    //
    // })
    this._willBlueSubscription =  navigation.addListener('willBlur', () => {
      Picker.hide()
      this.state.picker = false
    })
    let timeArr = []
    for(let i=7;i>1;i--){
      timeArr.push(moment().subtract(i, 'days').format('MM.DD'))
    }
    this.xAxis = [...timeArr,'昨天']
  }
  componentWillUnmount () {
    this._didFocusSubscription && this._didFocusSubscription.remove()
    this._willFocusSubscription && this._willFocusSubscription.remove()
    this._willBlueSubscription && this._willBlueSubscription.remove()
  }
  xAxis = []
  buttonArea = {
    top:pxToDp(20),
    right:pxToDp(20),
    bottom:pxToDp(20),
    left:pxToDp(20)
  }
  setDeviceName = ()=>{
    //设置设备名称
    const { actions, publicReducer, navigation } = this.props
    let {uid} = publicReducer.myInfo
    let mac = navigation.getParam ('mac')
    let {name} = this.state
    name = name || mac
    let body = {uid,mac,name}
    this.setState({edit:false})
    actions.setDeviceName(body)
  }

  sortWeekData = (datas) => {
    let dates = []
    for(let i=7;i>0;i--){
      dates.push(moment().subtract(i, 'days').format('YYYY-MM-DD'))
    }
    let pm25 = new Array(7).fill(0),pm10 = new Array(7).fill(0),aqi = new Array(7).fill(0),formaldehyde = new Array(7).fill(0)
    datas.map(data=>{
      dates.map((date,index)=>{
        if(data.day === date){
          pm25[index] = data.pm2_5
          pm10[index] = data.pm10
          aqi[index] = data.aqi
          formaldehyde[index] = data.formaldehyde
        }
      })
    })
    this.weekData = {pm25,pm10,aqi,formaldehyde}
  }

  open = (work,powerModel)=>{  //work 1 开机 2待机 3关机 powerModel 1 内部电源 2 外部电源
    const { actions, navigation, publicReducer } = this.props
    const deviceType = navigation.getParam('type') || 1   //1是醛扇E1,2是醛三E2
    const status = navigation.getParam('status') || 0   //0 离线 1 在线
    const mac = navigation.getParam ('mac')
    if(deviceType==='1'){
      this.setOrder(4-work)
      return
    }
    if(work===1){
      Picker.init({
        pickerData: ['关机','待机'],
        pickerTitleText:'',
        pickerConfirmBtnText:"确定",
        pickerCancelBtnText:"取消",
        pickerBg:[255, 255, 255, 1],
        onPickerConfirm: data => {
          this.setState({picker:false})
          this.setOrder(data[0]==='关机'?3:2)
        },
        onPickerCancel: ()=>{
          this.setState({picker:false})
        }
      })
      this.setState({picker:true})
      Picker.show()
    }
    if(work===2){
      if(powerModel===2){
        this.setOrder(1,1000)
      } else {
        actions.getDeviceStatus(mac).then(res=>{
          if(res&&res.powerModel === 1){
            Toast.showShortCenter("请将设备接入外部电源")
          }else{
            this.setOrder(1,1000)
          }
        }).catch(err=>{
          Toast.showShortCenter("请将设备接入外部电源")
        })
      }
    }
    if(work===3){
      if(powerModel===2){
        this.setOrder(1,1000)
      } else {
        actions.getDeviceStatus(mac).then(res=>{
          if(res&&res.powerModel === 1){
            Toast.showShortCenter("请将设备接入外部电源")
          }else{
            this.setOrder(1,1000)
          }
        }).catch(err=>{
          Toast.showShortCenter("请将设备接入外部电源")
        })
      }
    }

  }
  setOrder = ( power, time ) => {
    const { navigation, actions, publicReducer } = this.props
    let mac = navigation.getParam('mac')
    let type = navigation.getParam('type')
    let body = {
      mac,
      type,
      ...publicReducer.orderParams,
      power
    }
    actions.setOrderParam(body,time)
  }
  weekData = {}
  getPMLevel = (pm) => {
    if(pm<=35){
      return '优'
    }else if(pm>=36&&pm<=75){
      return '良'
    }else if(pm>=76&&pm<=115){
      return '轻度污染'
    }else if(pm>=116&&pm<=150){
      return '中度污染'
    }else if(pm>=151&&pm<=250){
      return '重度污染'
    }else if(pm>=251&&pm<=999){
      return '严重污染'
    }
  }
  _renderItem = ({type},key) => {  //type 0 PM传感器  1 甲醛传感器
    const {navigation,publicReducer} = this.props
    const mac = navigation.getParam ('mac')
    const {name,edit} = this.state
    const deviceType = navigation.getParam('type') || 1   //1是醛扇,2是醛三
    const {nowData,weekData,deviceStatus,orderParams} = publicReducer
    this.sortWeekData(weekData)
    console.log(deviceStatus)
    let { work, electric, formaldehyde_sensor, formaldehyde_sensor_alarm, pm25_sensor, pm25_sensor_alarm, powerModel } = deviceStatus
    let { aqi='0',c6h6,co,co2,formaldehyde='0',humidity='0',pm1_0,pm2_5='0',pm10='0',temperature=0,so2 } = nowData
    let { model } = orderParams
    let modelValue = ['睡眠','微风','清凉','强劲'][model]  //model 0表示睡眠模式 1表示微风模式 2表示清凉模式 3表示强劲模式
    electric = electric || 0
    powerModel = powerModel || 2
    formaldehyde_sensor_alarm = formaldehyde_sensor_alarm || 0
    pm25_sensor_alarm = pm25_sensor_alarm || 0
    formaldehyde_sensor = formaldehyde_sensor || 0
    pm25_sensor = pm25_sensor || 0
    work = work || 1
    const batteryIcon = [
      require('../images/sensor/battery0.png'),
      require('../images/sensor/battery1.png'),
      require('../images/sensor/battery2.png'),
      require('../images/sensor/battery3.png'),
      require('../images/sensor/battery4.png'),
      require('../images/sensor/battery5.png'),
    ]
    const warnIcon = [
      require('../images/sensor/unwarning.png'),
      require('../images/sensor/warning.png'),
    ]
    const switchIcon = [
      require('../images/sensor/switchOpen.png'),
      require('../images/sensor/switchWait.png'),
      require('../images/sensor/switchClose.png')
    ]
    let warnSource = !type ? warnIcon[pm25_sensor_alarm] : warnIcon[formaldehyde_sensor_alarm]  //显示左一报警图标
    const stateIcon = [
      require('../images/sensor/normal.png'),
      require('../images/sensor/ill.png'),
    ]
    let stateSource = !type ? stateIcon[pm25_sensor] : stateIcon[formaldehyde_sensor]  //显示左二状态图标
    let pmWeekData = [
      {
        name: 'AQI',
        type: 'line',
        smooth: true,
        symbol: 'circle',
        symbolSize: pxToDp(10),
        data: this.weekData.aqi
      },{
        name: 'PM2.5',
        type: 'line',
        smooth: true,
        symbol: 'circle',
        symbolSize: pxToDp(10),
        data: this.weekData.pm25
      },{
        name: 'PM10',
        type: 'line',
        symbol: 'circle',
        symbolSize: pxToDp(10),
        smooth: true,
        data: this.weekData.pm10
      }
    ]
    let chWeekData = [
      {
        name: 'CH₂O',
        type: 'line',
        symbol: 'circle',
        symbolSize: pxToDp(16),
        smooth: true,
        data: this.weekData.formaldehyde
      }
    ]
    let option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'none',
          lineStyle: {
            color:'#fff'
          }
        },
        position:['70%','0%'],
        confine:false,
        textStyle:{
          fontSize:pxToDp(20)
        }
      },
      legend: {
        data:type?['CH₂O']:['AQI','PM2.5','PM10'],
        align:'left',
        itemWidth: pxToDp(16),
        itemHeight: pxToDp(16),
        itemGap: pxToDp(26),
        selectedMode: false,
        textStyle:{
          color:'#fff',
          fontSize:pxToDp(24),
        },
        icon: 'circle',
        left:pxToDp(81),
        top:pxToDp(9)
      },
      grid: {
        left: pxToDp(33),
        right: pxToDp(48),
        top: pxToDp(60),
        containLabel: true
      },
      xAxis: {
        data: this.xAxis,
        boundaryGap:true,
        axisTick:{
          alignWithLabel:true
        },
        axisLine:{
          lineStyle:{
            color:'#fff'
          }
        }
      },
      textStyle:{
        color:'#fff',
        fontSize:pxToDp(24),
      },
      yAxis: {
        axisLine:{
          show:false,
        },
        splitNumber:6,
        axisTick:{
          show:false,
        },
        splitLine:{
          show:false,
        }
      },
      color: type?['rgb(216,255,0)']:['#FFBCF9','#E5F4FF', '#48FFE2'],
      series: type?chWeekData:pmWeekData
    }
    let quality = this.getPMLevel(pm2_5)
    return (<View key={key} style={{width:pxToDp(750),height:'100%'}}>
      <LinearGradient colors={type?['#C492FD', '#57ADEE']:['#75C6FF', '#57ADEE']} style={{width:pxToDp(750),height:'100%'}} >
        <View style={styles.header} >
          <TouchableOpacity style={{width:pxToDp(20),height:pxToDp(36),left:pxToDp(33)}} onPress={()=>navigation.goBack()} hitSlop={this.buttonArea} >
            <Image source={require('../images/sensor/left.png')} style={{width:pxToDp(20),height:pxToDp(36)}} />
          </TouchableOpacity>
          {edit?
            <TextInput
              ref='edit'
              style={{height: pxToDp(100),color:'#fff',fontSize:pxToDp(32)}}
              onChangeText={(name) => this.setState({name})}
              value={this.state.name}
              maxLength={8}
              autoFocus={type===this.state.type}
              underlineColorAndroid = 'transparent'
              blurOnSubmit
              onBlur={this.setDeviceName}
            />:
            <Text style={{color:'#fff',fontSize:pxToDp(32)}}>{name}</Text>
          }
          <TouchableOpacity style={{width:pxToDp(34),height:pxToDp(34),right:pxToDp(232)}} onPress={()=>{this.setState({type,edit:!edit})}} hitSlop={this.buttonArea} >
            <Image source={require('../images/sensor/edit.png')} style={{width:pxToDp(34),height:pxToDp(34)}} />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1,alignItems:'center'}}>
          <View style={{width:pxToDp(432),height:pxToDp(432),alignItems:'center',marginTop:pxToDp(66)}}>
            <Image source={require('../images/sensor/aqi.png')} style={{width:pxToDp(432),height:pxToDp(432),position:'absolute'}} />
            {type===0?<Text style={{marginTop:pxToDp(120),fontSize:pxToDp(32),color:'#3DA1EA'}}>PM2.5</Text>:null}
            {type===1?<Text style={{marginTop:pxToDp(120),fontSize:pxToDp(32),color:'#3DA1EA'}}>CH₂O</Text>:null}
            <Text style={{marginTop:pxToDp(5),fontSize:pxToDp(24),color:'#3DA1EA'}}>{type?'（mg/m³）':'（ug/m³）'}</Text>
            {type===1?<Text style={{marginTop:pxToDp(11),fontSize:pxToDp(68),color:'#3DA1EA'}}>{formaldehyde}</Text>:null}
            {type===0?<Text style={{marginTop:pxToDp(11),fontSize:pxToDp(68),color:'#3DA1EA'}}>{pm2_5}</Text>:null}
            {type===0?<Text style={{marginTop:pxToDp(1),fontSize:pxToDp(32-quality.length*2),color:'#3DA1EA'}}>{quality}</Text>:null}
          </View>
          <Image source={warnSource} style={{width:pxToDp(54),height:pxToDp(54),position:'absolute',top:pxToDp(10),left:pxToDp(20)}} />
          <Image source={stateSource} style={{width:pxToDp(54),height:pxToDp(54),position:'absolute',top:pxToDp(78),left:pxToDp(20)}} />
          {deviceType==2 && powerModel==1?<Image source={batteryIcon[electric]} style={{width:pxToDp(50),height:pxToDp(54),position:'absolute',top:pxToDp(10),right:pxToDp(20)}} />:null}
          {deviceType==2 && powerModel==2?<Image source={require('../images/sensor/outPower.png')} style={{width:pxToDp(50),height:pxToDp(54),position:'absolute',top:pxToDp(10),right:pxToDp(20)}} />:null}
          {deviceType==1?<Text style={{color:'#fff',fontSize:pxToDp(32),position:'absolute',top:pxToDp(10),right:pxToDp(20)}} >{modelValue||'--'}</Text>:null}
          <View style={{flexDirection:'row',position:'absolute',top:pxToDp(89),width:pxToDp(100),right:pxToDp(-8),alignItems:'center'}}>
            <Image source={require('../images/sensor/temperature.png')} style={{width:pxToDp(28),height:pxToDp(28)}} />
            <Text style={{fontSize:pxToDp(32),color:'#fff'}}>{temperature}°</Text>
          </View>
          <View style={{flexDirection:'row',position:'absolute',top:pxToDp(149),width:pxToDp(100),right:pxToDp(-8),alignItems:'center'}}>
            <Image source={require('../images/sensor/humidity.png')} style={{width:pxToDp(28),height:pxToDp(28)}} />
            <Text style={{fontSize:pxToDp(32),color:'#fff'}}>{humidity}</Text>
            <Image source={require('../images/sensor/hundred.png')} style={{width:pxToDp(20),height:pxToDp(28),alignSelf:'flex-start'}} />
          </View>
          {type===0?<View style={{flexDirection:'row',alignItems:'center',marginTop:pxToDp(17)}}>
            <Text style={styles.text}>PM10<Text style={styles.value}>{' '+pm10}</Text>ug/m³</Text>
            <Text style={[styles.text,{marginLeft:pxToDp(128)}]}>AQI <Text style={styles.value}>{' '+aqi}</Text></Text>
          </View>:<View style={{height:pxToDp(60)}}></View>}
          <View style={{height:pxToDp(level()*40+480),width:pxToDp(750),marginTop:pxToDp(level()*20+4)}}>
            <Text style={{color:'#fff',fontSize:pxToDp(32),marginLeft:pxToDp(33),marginBottom:pxToDp(level()*20-10)}}>过去一周监测数据</Text>
            <Echarts option={option} height={pxToDp(level()*40+460)}  />
          </View>
        </View>
        <View style={{width:pxToDp(750),height:pxToDp(147),backgroundColor:'rgb(103,181,239)',position:'absolute',bottom:0,zIndex:pxToDp(-10)}}>
          <TouchableOpacity style={{width:pxToDp(76),height:pxToDp(76),position:'absolute',top:pxToDp(23),left:pxToDp(80)}} onPress={()=>this.open(work,powerModel)} >
            <Image source={switchIcon[work-1]} style={{width:pxToDp(76),height:pxToDp(76)}} />
          </TouchableOpacity>
          <TouchableOpacity style={{width:pxToDp(79),height:pxToDp(76),position:'absolute',top:pxToDp(23),right:pxToDp(80)}} onPress={()=>navigation.navigate('SetDevice',{type:deviceType,mac})} >
            <Image source={require('../images/sensor/set.png')} style={{width:pxToDp(79),height:pxToDp(76)}} />
          </TouchableOpacity>
          <TouchableOpacity style={{position:'absolute',left:pxToDp(84),width:pxToDp(70),bottom:pxToDp(13),justifyContent:'center',alignItems:'center'}} onPress={()=>this.open(work,powerModel)} >
            <Text style={{fontSize:pxToDp(20),color:'#fff'}}>{work===2?'待机中':'开关'}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{position:'absolute',right:pxToDp(98),bottom:pxToDp(13)}} onPress={()=>navigation.navigate('SetDevice',{type:deviceType,mac})} >
            <Text style={{fontSize:pxToDp(20),color:'#fff'}}>设置</Text>
          </TouchableOpacity>
        </View>
      </LinearGradient>
    </View>)
  }
  sensors = [
    {type:0},
    {type:1}
  ]   //0 PM传感器  1 甲醛传感器
  render () {
    return (
      <ScrollView ref={'scroll'} style={styles.container} horizontal showsHorizontalScrollIndicator={false} snapToInterval={pxToDp(750)} snapToAlignment={'start'}
                  scrollEventThrottle = {40}
                  pagingEnabled
                  snapToOffsets = {this.state.type*pxToDp(750)}
      >
        {this.sensors.map((sensor,index)=>this._renderItem(sensor,index))}
        <Mask show={this.state.picker} />
      </ScrollView>
    )
  }
}
const marginTop = getStatusBarHeight(true)
const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection: 'row'
  },
  header: {
    marginTop,
    height:pxToDp(88),
    width:pxToDp(750),
    flexDirection:'row',
    alignItems:'center',
    backgroundColor:'transparent',
    justifyContent:'space-between'
  },
  text:{
    color:'#fff',
    fontSize:pxToDp(26),
  },
  value:{
    color:'#fff',
    fontSize:pxToDp(36),
    paddingLeft: pxToDp(13)
  },
})
const mapStateToProps = (state) => {
  const { publicReducer } = state
  return { publicReducer }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...userActions }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sensor)
