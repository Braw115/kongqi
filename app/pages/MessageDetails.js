import React from 'react'
import { ScrollView, View, StyleSheet, Text } from 'react-native'
import Header from '../components/Header/Header'
import pxToDp from '../utils/pxToDp'

class MessageDetails extends React.Component {
  render () {
    const { navigation } = this.props
    const { messageTitle, createTime, messageContent } = navigation.getParam('msg')
    return (
      <View style={styles.container}>
        <Header
          back={() => navigation.goBack()}
          title={messageTitle}
        />
        <ScrollView style={styles.container} >
          <View style={styles.textBox}>
            <Text style={styles.title}>{messageTitle}</Text>
            <Text style={styles.time}>{createTime}</Text>
            <Text style={styles.content}>{messageContent}</Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  textBox: {
    marginTop: pxToDp(60),
    marginHorizontal: pxToDp(30)
  },
  msgText: {
    paddingVertical: pxToDp(10)
  },
  title: {
    fontSize: pxToDp(32),
    color: '#333'
  },
  time: {
    fontSize: pxToDp(24),
    color: '#999',
    marginTop: pxToDp(10)
  },
  content: {
    fontSize: pxToDp(28),
    color: '#666',
    lineHeight: pxToDp(40),
    marginTop: pxToDp(40)
  }
})

export default MessageDetails
