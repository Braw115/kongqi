import React, { Component } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native'
import NavigatorService from './navigatorService'
import Home from './pages/Home'
import PasswordChange from './pages/PasswordChange'
import PhoneChange from './pages/PhoneChange'
import PasswordInfoChange from './pages/PasswordInfoChange'
import MailBox from './pages/MailBox'
import Register from './pages/Register'
import Login from './pages/Login'
import Message from './pages/Message'
import MessageDetails from './pages/MessageDetails'
import HistoryRecord from './pages/HistoryRecord'
import AboutUs from './pages/AboutUs'
import BindDevice from './pages/BindDevice'
import PersonInfo from './pages/PersonInfo'
import Splash from './pages/Splash'
import ChooseDevice from './pages/ChooseDevice'
import Aside from './components/Aside'
import { createStackNavigator, createDrawerNavigator, SafeAreaView } from 'react-navigation'
// 侧边栏我的消息
const MessageStack = createStackNavigator(
  {
    Message: Message,
    MessageDetails: MessageDetails
  },
  {
    initialRouteName: 'Message',
    mode: 'card',
    headerMode: 'none'
  }
)

MessageStack.navigationOptions = () => ({
  drawerLockMode: 'locked-closed'
})

// 侧边栏历史记录
const HistoryStack = createStackNavigator(
  {
    HistoryRecord: HistoryRecord
  },
  {
    initialRouteName: 'HistoryRecord',
    mode: 'card',
    headerMode: 'none'
  }
)

HistoryStack.navigationOptions = () => ({
  drawerLockMode: 'locked-closed'
})

// 侧边栏关于我们
const AboutUsStack = createStackNavigator(
  {
    AboutUs: AboutUs
  },
  {
    initialRouteName: 'AboutUs',
    mode: 'card',
    headerMode: 'none'
  }
)

AboutUsStack.navigationOptions = () => ({
  drawerLockMode: 'locked-closed'
})

const CustomDrawerContentComponent = (props) => {
  return (
    <ScrollView >
      <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
        <Aside parent={props} />
      </SafeAreaView>
    </ScrollView>
  )
}

// 侧边栏跳转页面
const DrawerNavigator = createDrawerNavigator(
  {
    Home: Home,
    HistoryStack: HistoryStack,
    MessageStack: MessageStack,
    AboutUsStack: AboutUsStack
  },
  {
    initialRouteName: 'Home',
    contentComponent: CustomDrawerContentComponent
  }
)

const LoginStack = createStackNavigator(
  {
    Register: Register,
    PasswordChange: PasswordChange,
    Login: Login
  },
  {
    initialRouteName: 'Login',
    mode: 'card',
    headerMode: 'none'
  }
)

// 主页跳转页面
const AppNavigator = createStackNavigator(
  {
    Drawer: DrawerNavigator,
    ChooseDevice: ChooseDevice,
    Login: LoginStack,
    PhoneChange: PhoneChange,
    PasswordInfoChange: PasswordInfoChange,
    PersonInfo: PersonInfo,
    MailBox: MailBox,
    BindDevice: BindDevice,
    Splash: Splash
  },
  {
    initialRouteName: 'Splash',
    mode: 'card',
    headerMode: 'none'
  }
)

class App extends Component {
  render () {
    return (
      <View style={styles.container}>
        <AppNavigator ref={navigatorRef => {
          NavigatorService.setNavigator(navigatorRef)
        }} />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
export default App
